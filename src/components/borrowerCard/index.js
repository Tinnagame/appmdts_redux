import React from 'react'
import { View, Text } from 'react-native'
import { Card, Icon } from 'react-native-elements'

const BorrowerCard = () => {
  return (
    <Card
      // key={user.userTel}
      containerStyle={{
        borderRadius: 10,
        borderWidth: 0,
        elevation: 8,
      }}
      wrapperStyle={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      <View>
        <Text style={{ marginBottom: 8, fontSize: 16 }}>
          {/* {user.userName} ({user.borrowingAmount}) */}
        </Text>
        <Text style={{ marginBottom: 8, fontSize: 16, color: '#6B6D71' }}>
          {/* {user.userTel} */}
        </Text>
      </View>
      <View>
        <Icon
          name="chevron-forward-outline"
          type="ionicon"
          color="black"
          // style={styles.actionButtonIcon}
        />
      </View>
    </Card>
  )
}

export default BorrowerCard
