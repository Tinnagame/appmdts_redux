import React, { useState, useEffect } from 'react'
import { View, Button, Text, Alert } from 'react-native'

import { useNavigation } from '@react-navigation/native'
import {pushNoti} from '../../configs/pushNotification'
import auth from '@react-native-firebase/auth'
import { db, userRef } from '../../configs/firestore'
import { authIsReady } from 'react-redux-firebase'
import QRCode from 'react-native-qrcode-svg'
import { WebView } from 'react-native-webview'
import cancelLocalNotifications from "react-native-push-notification"
import { TouchableOpacity } from 'react-native-gesture-handler'
import PushNotification from 'react-native-push-notification'
const Profile = () => {
  const navigation = useNavigation()
  
  //const {uid} = route.params
  //console.log(uid)
  const user = auth().currentUser
  //console.log(user.uid)

  const logout = () => {
    Alert.alert(
      'Confirm',
      'Are you want to log out?',[{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      },
      {text:'Logout', onPress: () => {auth().signOut().then(console.log('logout')),
      navigation.navigate('Login',{userToken:null})}}
    ],{ cancelable: false }
    )
  }
  const [asd, setasdList] = useState([])
  const [name, setname] = useState([])
  const [tel, setTel] = useState([])
  const [role, setRole] = useState('')
  
  useEffect(() => {
    const UserRef = userRef.child(user.uid)
    UserRef.on('value', (snapshot) => {
      const ref = snapshot.val()
      setname(ref.userName)
      setTel(ref.userTel)
      setRole(ref.userRole)
     // console.log('user.tel >> ', ref.tel)
    })

    //console.log(UserRef)s
  }, [])

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        // justifyContent: 'center',
        paddingTop: 50,
        alignItems: 'center',
        backgroundColor: '#ffffff'
      }}>
      <View style={{ flex: 0.5, marginBottom: 50 }}>
        <TouchableOpacity onPress={()=>{pushNoti("admin",user.uid)/*cancelLocalNotifications({id: 'u0a750'});*/}}>
        <QRCode value={user.uid} size={180} />
        </TouchableOpacity>
      </View>
      <View style={{ flex: 0.45 }}>
        {/*<Text style={{fontSize:20}}>Role: {role}</Text> */}
        <Text style={{ fontSize: 20, marginBottom: 10 }}>Name: {name}</Text>
        <Text style={{ fontSize: 20, marginBottom: 24 }}>
          Phone number: {tel}
        </Text>
      </View>
      <View style={{ width: '70%', justifyContent: 'center' }}>
        <Button color="#2C9086" title="logout" onPress={logout} />
        {/* <Button title="uid" onPress={() => console.log("UID => ", user.uid)} /> */}
      </View>
    </View>
  )
}

export default Profile
