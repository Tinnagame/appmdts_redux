import React, { useState, useEffect } from 'react'
import { View, Text, Button, TextInput, StyleSheet, Alert } from 'react-native'
import { Input } from 'react-native-elements'
import { NavigationHelpersContext, useNavigation } from '@react-navigation/native'
import database from '@react-native-firebase/database'
import * as Animatable from 'react-native-animatable'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'

import auth from '@react-native-firebase/auth'
import { authIsReady } from 'react-redux-firebase'
import { userRef } from '~/configs/firestore'
import { set } from 'lodash'
import BorrowerDeviceBorrowHistory from '../borrower/BorrowerDeviceBorrowHistory'
import { TouchableOpacity } from 'react-native-gesture-handler'

const LoginPage = () => {
  const navigation = useNavigation()
  const [user, setUser] = useState()
  const [initializing, setInitializing] = useState(false)
  const [userToken, setUserToken] = useState()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const [isEmpty, setIsEmpty] = useState(false)
  const [isIncorrect, setIsIncorrect] = useState(false)

  // const reference = database().ref(`/User/${uid}`).once('value')
  // .then((snapshot) => {
  //   // console.log('User data: ', snapshot.val())
  // })

  useEffect(() => {
    //console.log("UID ",auth().currentUser.uid)
    checkUser()
  }, [])
  // eslint-disable-next-line react-hooks/exhaustive-deps

  //}, [])

  const checkUser = () => {
    console.log('current user data ', auth().currentUser)

    if (auth().currentUser != null) {
      userRef.child(auth().currentUser.uid).once('value', (snapshot) => {
        const userData = snapshot.val()
        console.log('userdata LOG >> ', userData)
        autoLogin(userData.userRole)
        setUserToken(auth().currentUser)
      })
    } else {
      console.log('LOG')
    }
  }
  /*
  const onAuthStateChanged = (userData) => {
    setUser(user)
    /*console.log('userData :>> ', userData)
     console.log('user UID', auth().currentUser.uid)
    initializing && setInitializing(false)
  }*/

  const autoLogin = (userRole) => {
    try {
      if (userRole === 'admin') {
        navigation.reset({
          index: 1,
          routes: [
            {
              name: 'HomeAdmin',
            },
          ],
        })
      } else {
        navigation.reset({
          index: 1,
          routes: [{ name: 'HomeBorrower' }],
        })
      }
    } catch (err) {
      Alert.alert(
        'ERROR',
        'Incorrect email/password',
        [
          {
            text: 'OK',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        { cancelable: false },
      )
      console.log('err :>> ', err)
    }
  }

  const _TEST_Login_CorrectAll = async (inputEmail, inputPassword) => {
    try {
      console.log('*************TEST*****************')
      console.log('*************LOGIN****************')
      console.log('INPUT Email  >> ', inputEmail)
      console.log('INPUT Password >>', imputPassword)
      console.log('EXPECT: ')
      const res = await auth().signInWithEmailAndPassword(email, password)
      console.log('res >>', res)
      //console.log('uid : => ' , res.user.uid)
      if (!!res.user.uid) {
        const reference = await database()
          .ref(`/User/${res.user.uid}`)
          .child('userRole')
          .once('value')

        //console.log('reference', (await reference).val())

        const userRole = await reference.val()
        if (userRole === 'admin') {
          navigation.reset({
            index: 1,
            routes: [
              {
                name: 'HomeAdmin',
              },
            ],
          })
        } else {
          navigation.reset({
            index: 1,
            routes: [{ name: 'HomeBorrower' }],
          })
        }

        // navigation.reset({
        //   index: 1,
        //   routes: [{ name: 'Home' }],
        // })
      }
    } catch (err) {
      setIsEmpty(false)
      setIsIncorrect(true)
      // Alert.alert(
      //   'ERROR',
      //   'Incorrect email/password',
      //   [
      //     {
      //       text: 'OK',
      //       onPress: () => console.log('Cancel Pressed'),
      //       style: 'cancel',
      //     },
      //   ],
      //   { cancelable: false },
      // )
      console.log('err :>> ', err)
    }
  }
  const login = async () => {
    try {
      //const res = await auth().signInWithEmailAndPassword("admin@mdts.com", "123456")
      const res = await auth().signInWithEmailAndPassword(email, password).catch(console.log("error"))
      console.log('res >>', res)
      //console.log('uid : => ' , res.user.uid)
      if (!!res.user.uid) {
        const reference = await database()
          .ref(`/User/${res.user.uid}`)
          .child('userRole')
          .once('value')

        //console.log('reference', (await reference).val())

        const userRole = await reference.val()
        if (userRole === 'admin') {
          navigation.reset({
            index: 1,
            routes: [
              {
                name: 'HomeAdmin',
              },
            ],
          })
        } else {
          navigation.reset({
            index: 1,
            routes: [{ name: 'HomeBorrower' }],
          })
        }

        // navigation.reset({
        //   index: 1,
        //   routes: [{ name: 'Home' }],
        // })
      }
    } catch (err) {
      
      setIsEmpty(false)
      setIsIncorrect(true)
      // Alert.alert(
      //   'ERROR',
      //   'Incorrect email/password',
      //   [
      //     {
      //       text: 'OK',
      //       onPress: () => console.log('Cancel Pressed'),
      //       style: 'cancel',
      //     },
      //   ],
      //   { cancelable: false },
      // )
      console.log('err :>> ', err)
    }
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <KeyboardAwareScrollView style={{ flex: 1, width: '100%' }}>
        <View
          style={{
            flex: 1,
            width: '80%',
            alignSelf: 'center',
            paddingTop: 128,
          }}>
          {/* <Text> Email</Text> */}
          <Text style={{ fontSize: 30, flex: 1 }}>Hello there!</Text>

          <Text style={{ fontSize: 28, marginTop: 25 }}>Welcome</Text>
          <Text style={{ fontSize: 15, marginBottom: 20 }}>
            Please sign in to continue
          </Text>

          {/* username input */}
          <Input
            placeholder="Email"
            keyboardType="email-address"
            value={email}
            onChangeText={setEmail}
          />

          {/* password input */}
          <Input
            placeholder="Password"
            secureTextEntry
            value={password}
            onChangeText={setPassword}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              // borderWidth: 1,
              height: 20,
            }}>
            {isEmpty ? (
              <Animatable.View animation="fadeInLeft" duration={1000}>
                <Text style={styles.alertText}>
                  Please input your email and password to login
                </Text>
              </Animatable.View>
            ) : null}
            {isIncorrect ? (
              <Animatable.View animation="fadeInLeft" duration={1000}>
                <Text style={styles.alertText}>
                  Incorrect username or password
                </Text>
              </Animatable.View>
            ) : null}
          </View>

          <View
            style={{
              flex: 1,
              marginTop: 10,
            }}>
            <Button
              color="#2C9086"
              title="login"
              onPress={() => {
                if (!email || !password) {
                  setIsEmpty(true)
                  setIsIncorrect(false)
                  // Alert.alert(
                  //   'ERROR',
                  //   'Please input your email and password to login',
                  //   [
                  //     {
                  //       text: 'OK',
                  //       onPress: () => console.log('Cancel Pressed'),
                  //       style: 'cancel',
                  //     },
                  //   ],
                  // )
                } else {
                  login()
                }
              }}
            />
            <View style={{ marginTop: 16 }}>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <Text style={{ alignSelf: 'center', color: '#2C9086' }}>
                  {' '}
                  Create an account here
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  )
}

export default LoginPage

const styles = StyleSheet.create({
  alertText: {
    color: 'red',
    alignSelf: 'center',
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 4,
    paddingRight: 4,
    borderColor: 'red',
  },
})
