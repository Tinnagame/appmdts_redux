import React, { useEffect, useState } from 'react';

import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,View
  
} from 'react-native';
import { useNavigation } from '@react-navigation/native'

import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { GET_ALLDEVICE, ValidateQRcode } from '~/Functions';



const RemoveDeviceQRcodeScanner = () => {
  const navigation = useNavigation()
  const [DeviceList,setDeviceList] = useState([])
  useEffect(() => {
    GET_ALLDEVICE(setDeviceList)
  },[])

  const onSuccess = (e) => {
    if(e.data != null && ValidateQRcode(e,DeviceList)) {
    navigation.replace("RemoveDevice",{device:e.data});
    }
     else{
       Alert.alert("Can't use this QR code","Please try again",[
         {text:'OK',
         onPress: () => {
           navigation.goBack(),
           console.log("PRESS")
         }
        }
       ])
     }
    //props.route.params.onScan({text:e})
    console.log(e.data)
    
    };

    return (
      
      <QRCodeScanner
        onRead={onSuccess}
        flashMode={RNCamera.Constants.FlashMode.off}
        
        /*
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>OK. Got it!</Text>
          </TouchableOpacity>
        }*/
      />
      
        
      
    );
  }

  export default RemoveDeviceQRcodeScanner

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});