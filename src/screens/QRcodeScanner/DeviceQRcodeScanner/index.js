import React,{useState,useEffect} from 'react';

import {
  StyleSheet,
  Text,
  TouchableOpacity,View
  
} from 'react-native';
import { useNavigation } from '@react-navigation/native'

import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import {GET_ALLDEVICE, ValidateQRcode} from '~/Functions'

const DeviceQRcodeScanner = () => {
  const navigation = useNavigation()
  const [DeviceList,setDeviceList] = useState([])
  useEffect(() => {
    GET_ALLDEVICE(setDeviceList)
  },[])
  const onSuccess = (e) => {
    
    
    if(e.data != null && ValidateQRcode(e,DeviceList)) {
      navigation.navigate('RegularBorrowingForm',{device:e.data});
      //props.route.params.onScan({text:e})
      console.log(e.data," from DeviceQRcodeScanner")
    }else {
      Alert.alert("Can't use this QR code","Please try again",[
        {text:'OK',
        onPress: () =>{
          navigation.goBack(),
          console.log("PRESS")}}
        ])
      
    }
    };

    return (
      
      <QRCodeScanner
        onRead={onSuccess}
        flashMode={RNCamera.Constants.FlashMode.off}
        
        
      />
      
        
      
    );
  }

  export default DeviceQRcodeScanner

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});