import React, { useEffect, useState } from 'react';

import {
  Alert,
  StyleSheet,
  Text,
  TouchableOpacity,View
  
} from 'react-native';
import { useNavigation } from '@react-navigation/native'

import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { deviceRef, userRef } from '~/configs/firestore';
import lodash from 'lodash'
import {GET_ALLUSER,GET_ALLDEVICE,ValidateQRcode} from '~/Functions'

const BorrowerQRcodeScanner = () => {
  const navigation = useNavigation()
  const [DeviceList,setDeviceList] = useState([])
  const [userList,setUserList] = useState([])

  
  useEffect(() => {
    GET_ALLDEVICE(setDeviceList)
    GET_ALLUSER(setUserList)
    //ValidateQRcode()
    console.log("Dl >> ",DeviceList)
  },[])

  const onSuccess = (e) => {
    if(e.data != null && ValidateQRcode(e,userList)) {
      navigation.replace('RegularBorrowingForm',{uid:e.data,device:null})
    }
    else {
      Alert.alert("Can't use this QR code","Please try again",[
      {text:'OK',
      onPress: () =>{
        navigation.goBack(),
        console.log("PRESS")}}
      ])
    }
    };

    return (
      
      <QRCodeScanner
        onRead={onSuccess}
        flashMode={RNCamera.Constants.FlashMode.off}
        
        /*
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>OK. Got it!</Text>
          </TouchableOpacity>
        }*/
      />
      
        
      
    );
  }

  export default BorrowerQRcodeScanner

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777'
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16
  }
});