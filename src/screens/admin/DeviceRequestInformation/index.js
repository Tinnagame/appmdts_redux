import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Modal, Alert} from 'react-native'

import { Card, Button } from 'react-native-elements'

import { useNavigation } from '@react-navigation/native'

import { db, borrowerRef,userRef } from '../../../configs/firestore'

import moment from 'moment'

const DeviceRequestInformation = (props) => {
  
  console.log('route :>> ', props.route)
  const navigation = useNavigation()
  /*const {key} = route.params
  const {id} = route.params
  const {model} = route.params
  const {amount} = route.params
  const {location} = route.params
  const {date} = route.params
  const {status} = route.params
  */

  const { key, id, deviceType, amount, location, requestingDate, requestingStatus,borrowerName,borrowerUID,borrowerTel } = props.route.params

  const [temp, setTempList] = useState([])

  const approveDevice = () => {
    console.log('approve press')
    addtoHistory("approved")
    //deleteFromFirebase()
    Alert.alert(
      "","Approve"
    )
  }

  const rejectDevice = () => {
    console.log('reject press')
    
    addtoHistory("rejected")
    //deleteFromFirebase()
    Alert.alert(
      "","Rejected"
    )
  }
  
  const deleteFromFirebase =() => {
    const tempBorrowRef = userRef.child(borrowerUID).child('Items')
  .child('requesting').child(key)
  //tempBorrowRef.remove()
  }
  const addtoHistory = (info) => {
    const today = moment().format('YYYY/MM/DD')
    
    
    const historyRef = db.ref('User').child(borrowerUID).child('History/RequestingHistory').child(today)
    historyRef.push({
      deviceUID: key,
      deviceType: deviceType,
      amount: amount,
      location: location,
      requestingDate: requestingDate,
      requestingStatus: info,
      borrowerName : borrowerName,
      borrowerTel: borrowerTel,
      borrowerUID: borrowerUID,
      
    })
  }
  //
  

  return (
    <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      <Card>
        <View>
          <Text style={styles.headerText}>Device name</Text>
          <Text style={styles.contentText}>{deviceType || ''}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Amount</Text>
          <Text style={styles.contentText}>{amount || ''}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Location</Text>
          <Text style={styles.contentText}>{location}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Date and Time</Text>
          <Text style={styles.contentText}>
            {moment(requestingDate).format('L') + ' ' + moment(requestingDate).format('LT')}
          </Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Borrower name</Text>
          <Text style={styles.contentText}>{borrowerName}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Borrower phone number</Text>
          <Text style={styles.contentText}>{borrowerTel}</Text>
        </View>
      </Card>
      {/*) : '' }*/}
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingLeft: 16,
          paddingRight: 16,
          marginTop: 24,
        }}>
        <View style={{ flex: 0.5, paddingRight: 4 }}>
          <Button
            title="REJECT"
            buttonStyle={{ backgroundColor: 'red' }}
            onPress={() => {
              navigation.navigate('Home'), rejectDevice()
            }}
          />
        </View>
        <View style={{ flex: 0.5, paddingLeft: 4 }}>
          <Button
            title="APPROVE"
            buttonStyle={{ backgroundColor: 'green' }}
            onPress={() => {  approveDevice() ,navigation.navigate('Home')}}
          />
        </View>
      </View>
      
    </View>
  )
}

export default DeviceRequestInformation

const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
})
