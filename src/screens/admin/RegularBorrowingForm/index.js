import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, ScrollView, Button, Alert } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { userRef, locationRef, deviceRef } from '~/configs/firestore'
import { Card, Input } from 'react-native-elements'
import { Picker } from '@react-native-community/picker'
import moment from 'moment'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { INIT_USER, INIT_LOCATION } from '~/Functions'
import { check } from 'react-native-permissions'
const RegularBorrowingForm = (props) => {
  const navigation = useNavigation()
  const { uid, device } = props.route.params

  const [borrowerName, setborrowerName] = useState('0')
  const [borrowerTel, setborrowerTel] = useState('0')
  const [locations, setLocationList] = useState([])
  const [selectedLocation, setSelectedLocation] = useState('0')
  const [serialNumber, setSerialNumber] = useState('')
  const [borrowingList, setBorrowingList] = useState([])
  const [text, setText] = useState('0')
  const [borrowingPathList, setBorrowingPathList] = useState([])
  const [devicePathList, setDevicePathList] = useState([])

  const checkLocation = selectedLocation != '0'
  const checkDevice = borrowingList.length != 0
//updateDeviceInformation
  const ChangeCurrentStatusInDatabase = (
    borrowingPathList,
    selectedLocation,
    uid,
    borrowerName,
    borrowerTel,
  ) => {
    for (let key in borrowingPathList) {
      console.log('key lopp in borrowingPathList >> ', borrowingPathList[key])
      deviceRef.child('deviceType/' + borrowingPathList[key]).update({
        currentStatus: 'In-use',
        currentLocation: selectedLocation,
        borrowerUID: uid,
        borrowerName: borrowerName,
        borrowerTel: borrowerTel,
        borrowingDate: moment().format('YYYY/MM/DD, LT'),
      })

      //AddInUseDataToDatabase
      deviceRef
        .child('deviceType/' + borrowingPathList[key])
        .once('value', (snapshot) => {
          const data = snapshot.val()
          console.log(data)
          userRef
            .child(uid)
            .child('Items/In-use/' + borrowingPathList[key])
            .update({
              UID: borrowingPathList[key],
              currentLocation: data.currentLocation,
              currentStatus: data.currentStatus,
              deviceBrand: data.deviceBrand,
              deviceModel: data.deviceModel,
              deviceSerialNumber: data.deviceSerialNumber,
              borrowingDate: moment().format('YYYY/MM/DD, LT'),
            })
        })
    }
  }
  const validateDevice = (device, DevicePathList) => {
    const newPathList = DevicePathList
    if (DevicePathList.length == 0 && device) {
      DevicePathList.push(device)
      return true

    }
    
    let deviceAlreadyExist = false
    for(let i=0;i< DevicePathList.length; i++) {
      if(deviceAlreadyExist) break;
      deviceAlreadyExist = device === DevicePathList[i]
    }

    if (!deviceAlreadyExist) {
      newPathList.push(device)
      setDevicePathList(newPathList)
      return true
    }
    Alert.alert('', 'This device is already added')

    return false
    
  }

  const FetchData = () => {
    if (device && validateDevice(device, devicePathList)) {
      deviceRef.child('deviceType/' + device).once('value', (snapshot) => {
        const ref = snapshot.val()
        const deviceType = device.split('/')[0]
        if(ref){
        let tmpBL = [...borrowingList]
          tmpBL.push({ deviceType, ...ref })
          let tmpBPL = [...borrowingPathList]
          tmpBPL.push(device)

          setBorrowingList(tmpBL)
          //TEST
          console.log("EXPECT Borrowing List >>>>> ", tmpBL)
          console.log("EXPECT BorrowingPath List", tmpBPL)
          setBorrowingPathList(tmpBPL)
        }
      })
    } else{
      console.log("cannot fetch device")
    }
  }
  useEffect(() => {
    INIT_USER(uid, setborrowerName, setborrowerTel)
    INIT_LOCATION('Location', setLocationList)
    FetchData()
  }, [device])

  return (
    <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      <Card>
        <View style={{ marginTop: 0 }}>
          <Text style={styles.headerText}>Borrower name</Text>
          <Text style={styles.contentText}>{borrowerName || '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Borrower phone number</Text>
          <Text style={styles.contentText}>{borrowerTel || '-'}</Text>
        </View>
        <View style={{ marginTop: 16 }}>
          <Text style={styles.pickerLabel}> Location </Text>
          <Picker
            style={{ height: 50, width: '100%' }}
            mode="dropdown"
            selectedValue={selectedLocation}
            onValueChange={(itemValue) => {
              setSelectedLocation(itemValue)
            }}>
            <Picker.Item label="select location" value="0" />
            {locations
              ? locations.map((location, index) => (
                  <Picker.Item
                    label={location.name}
                    value={location.name}
                    key={location.name}
                  />
                ))
              : ''}
          </Picker>
        </View>
      </Card>

      <View style={{ marginTop: 10 }}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('DeviceQRcodeScanner')
          }}>
          <Card>
            <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  fontSize: 20,
                  paddingLeft: 100,
                }}>
                ADD DEVICE
              </Text>
              <Icon name="qrcode-scan" size={30}></Icon>
            </View>
          </Card>
        </TouchableOpacity>
      </View>

      <ScrollView style={{ marginTop: 16, flex: 1 }}>
        {!!borrowingList.length &&
          borrowingList.map((device, index) => (
            <TouchableOpacity>
              <Card key={device.deviceSerialNumber}>
                <View>
                  <Text style={{ paddingBottom: 2 }}>
                    {device.deviceType || '-'}
                  </Text>
                  <Text style={{ color: 'gray' }}>Device Model</Text>
                  <Text>{device.deviceModel || '-'}</Text>
                  <Text style={{ color: 'gray' }}>S/N</Text>
                  <Text> {device.deviceSerialNumber || '-'}</Text>
                </View>
              </Card>
            </TouchableOpacity>
          ))}
      </ScrollView>
      <View>
        <Button
          disabled={!checkLocation || !checkDevice}
          title="BORROW"
          color="#2C9086"
          onPress={() => {
            ChangeCurrentStatusInDatabase(
              borrowingPathList,
              selectedLocation,
              uid,
              borrowerName,
              borrowerTel,
            ),
              Alert.alert('', ' Borrowing Complete'),[{
                text:'OK',
                onPress: () => navigation.navigate('Home')
              }]
              
          }}
        />
      </View>
    </View>
  )
}
export default RegularBorrowingForm
const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
})
