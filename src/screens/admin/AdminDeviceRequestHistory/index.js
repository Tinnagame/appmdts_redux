import React ,{useState,useEffect} from "react";
import { View, Text, StyleSheet,TouchableOpacity,RefreshControl} from "react-native";
import auth from '@react-native-firebase/auth'
import {userRef} from '../../../configs/firestore'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import { Card } from 'react-native-elements'
import { ScrollView } from "react-native-gesture-handler";
import Constants from 'expo-constants';


const AdminDeviceRequestHistory = () => {
    const [] = useState([]);
    const [selectHistoryList,setSelectHistoryList] = useState([])
    const [selectedDate] = useState(moment().format('YYYY/MM/DD'))
    const [] = useState('')
    const [date, setDate] = useState(Date.parse(moment().format('L')))
    const [mode, setMode] = useState('date')
    const [show, setShow] = useState(false)

    //refresh control
    const [refreshing,setRefreshing] = useState(false)
    
    const onRefresh = () => {
      setRefreshing(true);
      console.log('refresh >> ', refreshing)
      wait(1000).then(() => setRefreshing(false));
      
    }
    const wait = (timeout) => {
      return new Promise(resolve => {
        setTimeout(resolve, timeout);
      });
    }
    /*
    const wait = (timeout) => {
      return new Promise(resolve => {
        setTimeout(resolve, timeout);
      });
    }
*/
    const fetchData = () => {
      userRef.once('value',(snapshot) => {
        const userSnap = snapshot.val()
        const hist = [] 
        for(let key in userSnap) {
          if(userSnap[key].userRole == "borrower"){  
          const HistoryRef = userRef.child(key).child('History/RequestingHistory').child(moment(date).format('YYYY/MM/DD'))
            HistoryRef.once('value',(snapshot) => {
              const history = snapshot.val()
              for(let id in history) {
                hist.push({id,...history[id]})
              }
            })
          }
        }setSelectHistoryList(hist)
      })
    }
    
useEffect(() => {
  
  console.log("-------------------UseEffect---------------------- ")
    fetchData()
 },[date,refreshing]);



   const onChange = (event, selectingDate) => {
    const currentDate = selectingDate || date
    setShow(Platform.OS === 'ios')
    setDate(currentDate)
    setShow(false)
  }
   const showMode = (currentMode) => {
    setShow(true)
    setMode(currentMode)
  }
   const showDatepicker = () => {
    showMode('date')
  }
return(
    <View style={{flex:1, backgroundColor: '#f3f3f3'}}>
        <View style={{ backgroundColor: '#f3f3f3', flexDirection:'row'} }>
        <Text style={styles.sectionHeader}>device request history of </Text>
        <TouchableOpacity 
                onPress={showDatepicker}>
                <View style={ styles.sectionHeader} >
                  <View style={{flexDirection:'row',}}>
                    <Text style={{fontSize:16}}>
                      {moment(date).format('DD/MM/YYYY')}
                    </Text>
                  </View>
                </View>     
         </TouchableOpacity>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  timeZoneOffsetInMinutes={0}
                  mode={mode}
                  is24Hour={true}
                  display="spinner"
                  onChange={onChange}
                />
              )}
        </View>
        <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>} >
        
        {console.log(selectHistoryList)}
        {!!selectHistoryList.length &&
        selectHistoryList.map((Device) => (
          <TouchableOpacity
            key={Device.key}
            >
            <Card key={Device.key} >
              <View
                style={styles.card
                
                }>
                <View>
                  <Text style={{ marginBottom: 8 }}>
                    {Device.deviceType}
                  </Text>
                  <Text style={{ marginBottom: 8 }}>
                    amount: {Device.amount}
                  </Text>
                  <Text>
                    Date :{' '}
                    {moment(Device.date).format('L') +
                      ' ' +
                      moment(Device.date).format('LT')}
                  </Text>
                </View>
                <View>
                  <Text style={Device.requestingStatus=="approved" ? styles.approved : styles.rejected}>{Device.requestingStatus}</Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
        ))}
        </ScrollView>
        {!selectHistoryList.length &&
         <Text style={{flex:1,alignSelf:'center'}}>There is no History on this day</Text>}
   </View>
    )
}
const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  approved: {
    color: '#28CD1A'
  },
  rejected: {
    color: '#ff0000'
  },
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },
    sectionHeader: {
      color: 'grey',
      textTransform: 'uppercase',
      fontSize: 15,
      marginLeft: 16,
      marginTop: 16,
      marginRight: 16
    },
  })
export default AdminDeviceRequestHistory