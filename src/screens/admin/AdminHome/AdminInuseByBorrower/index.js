import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Button,RefreshControl
} from 'react-native'

import { Card, Icon } from 'react-native-elements'
import ActionButton from 'react-native-action-button'

import { useNavigation } from '@react-navigation/native'
import { map,filter, concat } from 'lodash'
import { db, borrowerRef, userRef ,deviceRef } from '../../../../configs/firestore'

import moment from 'moment'
import { auth } from '@react-native-firebase/auth'
import { color } from 'react-native-reanimated'
import { ScrollView } from 'react-native-gesture-handler'
//import { INIT_USER } from '~/Functions'
//temp

const AdminInuseByBorrower = (props) => {
  const navigation = useNavigation()
  const {uid,userName} = props.route.params
  //const {currentUser}

  // const {uid} = route.params
  //console.log(route)

 
const [DeviceTypeList,setDeviceTypeList] = useState([])
const [deviceList,setDeviceList] = useState([])
  //
  const [refreshing,setRefreshing] = useState(false)

  const onRefresh = () => {
    setRefreshing(true);

    wait(1000).then(() => setRefreshing(false));
  }

  const wait = (timeout) => {
    return new Promise(resolve => {
      setTimeout(resolve, timeout);
    });
  }
  const fetchData=() => {
    const Ref =  userRef.child(uid).child("Items/In-use");
    Ref
  .once('value',(snapshot) => {
    const newDeviceList =[]
    const borrowingListRef = snapshot.val();
    //console.log("vlg: ", borrowingListRef)
    for(let key in borrowingListRef) {
      console.log(key," >>> ",borrowingListRef[key])
      //newDeviceList.push(borrowingListRef[key])
      for(let id in borrowingListRef[key]) {
        console.log("ID ",id);
        console.log(borrowingListRef);
        newDeviceList.push({"deviceType":key.split("/")[0],...borrowingListRef[key][id]})
      }
    }
    setDeviceList(newDeviceList)
    console.log(deviceList)
  })
}
  
  useEffect(() => {
  
     fetchData()
  
  }, [refreshing])

  return (
    <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>

      {/*Request*/}
      <Text style={styles.sectionHeader}> Borrowing Device List </Text>
      {/* card*/}

      {/*try - mapped */}

<ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}>

{!!deviceList.length &&
        deviceList.map((Device, index) => (
          console.log("render >>",Device),
          <TouchableOpacity
            key={index}
            onPress={() =>console.log("")}>
            <Card
            containerStyle={{
              borderRadius: 10,
              borderWidth: 0,
              elevation: 8,
              marginBottom: 16,
            }}
            wrapperStyle={{
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                  
                <View>
                <Text style={{ marginBottom: 8 }}>
                    {Device.deviceType||''}
                </Text>
                  <Text style={{ marginBottom: 8 }}>
                    Brand : {Device.deviceBrand||''}
                </Text>
                <Text style={{ marginBottom: 8 }}>
                   Model :  {Device.deviceModel|| ''}
                </Text>
                <Text style={{ marginBottom: 8 }}>
                   S/N : {Device.deviceSerialNumber|| ''}
                </Text>
                <Text style={{ marginBottom: 8 }}>
                   Patient Name : {Device.patientName || "-"}
                </Text>
                <Text style={{ marginBottom: 8 }}>
                   Patient Hospital Number : {Device.patientHospitalNumber|| "-"}
                </Text>
                </View>
               
              </View>
            </Card>
          </TouchableOpacity>
              ))}
              
</ScrollView>
      {/*ACTION BUTTON*/}

    </View>
  )
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  sectionHeader: {
    color: 'grey',
    textTransform: 'uppercase',
    marginLeft: 16,
    marginTop: 16,
  },
})

export default AdminInuseByBorrower
