import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  RefreshControl,
  ScrollView,
} from 'react-native'

import { Card, Icon, Button } from 'react-native-elements'
import ActionButton from 'react-native-action-button'

import { useNavigation } from '@react-navigation/native'
import { map, filter, concat, upperCase } from 'lodash'
import { db, borrowerRef, userRef, deviceRef } from '../../../configs/firestore'

import moment from 'moment'
import { auth } from '@react-native-firebase/auth'
import { color } from 'react-native-reanimated'
// import { ScrollView } from 'react-native-gesture-handler'
import { Item } from 'native-base'
import { BlurView } from '@react-native-community/blur'
import { withSafeAreaInsets } from 'react-native-safe-area-context'

import BorrowerCard from './../../../components/borrowerCard'

const AdminHome = ({ route }) => {
  const navigation = useNavigation()
  //const {currentUser}

  // const {uid} = route.params
  //console.log(route)

  const [borrowers, setBorrowerList] = useState([])
  const [users, setUserList] = useState([])
  const [Ids, setIdList] = useState([])
  const [requestDevice, setRequestDeviceList] = useState([])
  const [tempRequest, setTempRequest] = useState([])
  const [borrowerDataList, setBorrowerDataList] = useState([])
  const [refreshing, setRefreshing] = useState(false)
  const onRefresh = () => {
    setRefreshing(true)
    wait(100).then(() => setRefreshing(false))
  }
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout)
    })
  }
  const fetchData = () => {
    userRef.on('value', (snapshot) => {
      console.log('ref ontime')
      const userSnap = snapshot.val()
      const mapUser = filter(userSnap, (user) => {
        const { Items, ...otherItem } = user
        if (!!Items) {
          const newBorrowTemp = map(Items.requesting, (temp) => ({
            ...temp,
          }))

          const newUser = {
            Items: {
              requesting: newBorrowTemp,
            },
            ...otherItem,
          }
          return newUser
        }
      })

      const test = []
      const mapItem = filter(mapUser, (item) => {
        const itemList = item.Items.requesting
        for (let key in itemList) {
          test.push({ key, ...itemList[key] })
        }
      })
      setRequestDeviceList(test)
      setBorrowerList()
    })
  }

  const fetchingInUse = () => {
    //const newData = []w

    userRef
      .once('value', (snapshot) => {
        console.log('SNAPP >>', snapshot.val())
        const userSnap = snapshot.val()
        const newData = []
        for (let UID in userSnap) {
          console.log(UID, ' : ', userSnap[UID].Items)

          if (userSnap[UID].Items) {
            for (let item in userSnap[UID].Items) {
              if (item == 'In-use') {
                let borrowingAmount = 0

                //console.log("yay >>", UID)
                userRef
                  .child(UID)
                  .child('Items/In-use')
                  .once('value', (snapshot) => {
                    const ref = snapshot.val()

                    for (let key in ref) {
                      //console.log("key => ",key)
                      if (ref[key]) {
                        console.log('ref >> ', ref[key])
                        for (let subkey in ref[key]) {
                          console.log('sub >> ', subkey)
                          borrowingAmount++
                        }
                        //newData.push Here
                      }
                    }
                    newData.push({
                      borrowingAmount: borrowingAmount,
                      ...userSnap[UID],
                      UID,
                    })
                  })
              }
            }
          }
        }
        //set
        console.log(borrowerDataList)
        setBorrowerDataList(newData)
      })
      .then(() => refresh)
  }
  const boom = () => {
    deviceRef.child("deviceType").once('value',(snapshot)=> {
      console.log(snapshot.val())
      const ref = snapshot.val()
      for(let type in ref) {
        deviceRef.child('deviceType/'+type).once('value',(snapshot) => {
          //console.log(snapshot.val())
          for(let device in snapshot.val()) {
            deviceRef.child('deviceType/'+type+"/"+device).update({
              registeredDate:"2020/09/10",
            currentStatus:"Returned",
            currentLocation:'Blood Gas Room'
            })
          }
        })
        
          
          /*deviceRef.child("deviceType/"+ref[type]).child(device).update({
            registedDate:"2020/09/10",
            currentStatus:"returned",
            currentLocation:'Blood Gas Room'
          })*/
        }
      
    })
  }
  useEffect(() => {
    //try retrieve borrower with multiple user

    fetchData()
    fetchingInUse()
  }, [refreshing])

  return (
    // Main app page
    <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      {/*Borrower*/}
      
      <Text
        style={{
          marginTop: 16,
          marginLeft: 16,
          textTransform: 'uppercase',
          fontSize: 16,
        }}>
        Borrower
      </Text>
      <View style={{ flex: 0.45 }}>
        <ScrollView
          contentContainerStyle={{ paddingBottom: 16 }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {!!borrowerDataList &&
            borrowerDataList.map(
              (user, index) => (
                console.log('USER >>', user),
                (
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('AdminInuseByBorrower', {
                        uid: user.UID,
                        userName: user.userName,
                      })
                    }}>
                     <Card
                      key={user.userTel}
                      containerStyle={{
                        borderRadius: 10,
                        elevation: 8,
                      }}
                      wrapperStyle={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                      }}>
                      <View>
                        <Text style={{ marginBottom: 8, fontSize: 16 }}>
                          {user.userName} ({user.borrowingAmount})
                        </Text>
                        <Text style={{ marginBottom: 8, fontSize: 16, color: '#6B6D71' }}>
                          {user.userTel}
                        </Text>
                      </View>
                      <View>
                        <Icon
                          name="chevron-forward-outline"
                          type="ionicon"
                          color="black"
                          // style={styles.actionButtonIcon}
                        />
                      </View>
                    </Card>
                  </TouchableOpacity>
                )
              ),
            )}
        </ScrollView>
      </View>
      {/*Request*/}
      <View style={{ flex: 0.5 }}>
        <Text
          style={{
            marginTop: 16,
            marginLeft: 16,
            textTransform: 'uppercase',
            fontSize: 16,
          }}>
          requesting
        </Text>
        <ScrollView contentContainerStyle={{ paddingBottom: 16 }}>
          {/* this card is a mock up */}
          {/*
          <Card
            containerStyle={{
              borderRadius: 10,
              borderWidth: 0,
              elevation: 8,
              marginBottom: 16,
            }}
            wrapperStyle={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View>
              <Text style={{ marginBottom: 8, fontSize: 16 }}>
                {' '}
                Device Type ** mock up **{' '}
              </Text>
              <Text style={{ marginBottom: 8, fontSize: 14, color: '#6B6D71' }}>
                {' '}
                Amount: 1{' '}
              </Text>
              <Text style={{ marginBottom: 8, fontSize: 14, color: '#6B6D71' }}>
                {' '}
                01/01/2020 10:00 AM{' '}
              </Text>
            </View>
            <View
              style={{
                backgroundColor: '#FFF8DE',
                borderRadius: 20,
                paddingHorizontal: 8,
                paddingVertical: 4,
              }}>}
              { <Button title="pending" containerStyle={{ borderRadius: 20, backgroundColor: 'black' }} titleStyle={{ color: '#FFC845', fontSize: 12, lineHeight: 12 }}></Button> }
              <Text style={{ color: '#FFC845' }}> pending </Text>
            {/*</View>*/}
          {/*</Card>*/}
           {!!requestDevice.length &&
            requestDevice.map(
              (Device, index) => (
                console.log('device >>', Device),
                (
                  <TouchableOpacity
                    key={index}
                    onPress={() =>
                      navigation.navigate('Device Request Information', {
                        key: Device.key,
                        deviceType: Device.deviceType,
                        amount: Device.amount,
                        location: Device.location,
                        requestingDate: Device.requestingDate,
                        requestingStatus: Device.requestingStatus,
                        borrowerName: Device.borrowerName,
                        borrowerTel: Device.borrowerTel,
                        borrowerUID: Device.borrowerUID,
                      })
                    }>
                    <Card
                    containerStyle={{
                        borderRadius: 10,
                        borderWidth: 0,
                        elevation: 8,
                      }}
                      wrapperStyle={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <View>
                          <Text style={{ marginBottom: 8 }}>
                            {Device.deviceType}
                          </Text>
                          <Text style={{ marginBottom: 8 }}>
                            amount: {Device.amount}
                          </Text>
                          <Text>
                            Date :{' '}
                            {moment(Device.requestingDate).format('L') +
                              ' ' +
                              moment(Device.requestingDate).format('LT')}
                          </Text>
                        </View>
                        <View style={{flexDirection:"row", paddingLeft:100}}>
                          <Text style={{ color: '#FFD700' }}>
                          {Device.requestingStatus}
                          </Text>
                        </View>
                      </View>
                    </Card>
                  </TouchableOpacity>
                )
              ),
            )}
        </ScrollView>
      </View>

      {/* Floating Action Button */}

      <ActionButton
        buttonColor="#2C9086"
        backdrop={
          <BlurView blurType="dark" style={styles.blur} blurAmount={3} />
        }>
        <ActionButton.Item
          buttonColor="#ffffff"
          title="MANAGER"
          onPress={() => {
            navigation.navigate('DeviceManager'),
              console.log('QR Press')
          }}>
          <Icon
            name="md-qr-code-outline"
            type="ionicon"
            color="black"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#ffffff"
          title="VIEW INFO"
          onPress={() => {
            navigation.navigate('DeviceViewerQRcodeScanner'),
              console.log('QR Press')
          }}>
          <Icon
            name="md-qr-code-outline"
            type="ionicon"
            color="black"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>

        <ActionButton.Item
          buttonColor="#EB5757"
          title="BORROW"
          onPress={() => {
            navigation.navigate('BorrowerQRcodeScanner'),
              console.log('QR Press')
          }}>
          <Icon
            name="arrow-up-outline"
            type="ionicon"
            color="white"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#F1B46C"
          title="RETURN"
          onPress={() => navigation.navigate('ReturningDeviceQRcodeScanner')}>
          <Icon
            name="arrow-down-outline"
            type="ionicon"
            color="white"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
      </ActionButton>
    </View>
  )
}

//----- stylesheet -----
const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  blur: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
})

export default AdminHome
