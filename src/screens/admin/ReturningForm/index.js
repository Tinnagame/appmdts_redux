import React , {useEffect,useState}from 'react'
import { View, Text,StyleSheet,ScrollView,Button, Alert } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { userRef ,locationRef, deviceRef} from '~/configs/firestore'
import { Card,Input,  } from 'react-native-elements'
import moment from 'moment'



import { map,set } from 'lodash'
const ReturningForm = (props) => {
 const navigation = useNavigation()
 const {device}= props.route.params
 
 const [borrowerName, setborroweName] = useState('0')
 const [borrowerTel, setborroweTel] = useState('0')
 const [borrowerUID,setBorrowerUID] = useState('0')
 const [borrowingList,setBorrowingList] = useState([])
 const [borrowingPathList,setBorrowingPathList] = useState([])
 const [returningDevice,setReturningDevice] = useState([])
 const [deviceData,setDeviceData] = useState([])
 const devicesList = []

const checkBorrower = borrowerName != undefined


  const ChangeCurrentStatusToReturnInDatabase = () => {
    deviceRef.child("deviceType/"+device).update({
      currentStatus: "Returned",
      currentLocation: "Blood Gas Room",
      returningTime: moment().format("YYYY/MM/DD, LT"),
      patientName:null,
      patientHospitalNumber:null
    })
  
    //remove device from in-used device
    
    userRef.child(borrowerUID).child("Items/In-use/"+device).remove()
    }

  
  const AddReturnDeviceToHistory = (path,borrowerUID,deviceData) => {
    console.log("ADDTOHIS")
    userRef.child(borrowerUID).child("History/BorrowingHistory").child(moment().format("YYYY/MM/DD")).child(borrowerUID).push(
      {
      UID:path,
      currentLocation: deviceData.currentLocation,
      currentStatus: "Returned",
      deviceBrand: deviceData.deviceBrand,
      deviceModel: deviceData.deviceModel,
      deviceSerialNumber : deviceData.deviceSerialNumber,
      returningTime: moment().format("YYYY/MM/DD, LT")
    })
      
    
  }

  
    const fetchData = () => {
      deviceRef.child("deviceType/"+device).once('value', (snapshot) => {
        const deviceTemp = snapshot.val()
        setDeviceData(deviceTemp)
        const device = []
        console.log(deviceTemp)
        setborroweName(deviceTemp.borrowerName)
        setborroweTel(deviceTemp.borrowerTel)
        setBorrowerUID(deviceTemp.borrowerUID)
        device.push({...deviceTemp})
        
        setReturningDevice(device)
      })
    }

  useEffect(() => {
    //console.log("blwn",borrowerName);
   // console.log("QRing device >> ",device)
    //console.log("BLI",borrowingList)

    //GET Device 
    fetchData()
    
    //SET Borrower
    /*userRef.child().once('value', (snapshot) => {
      const ref = snapshot.val()
      //console.log(ref)
      console.log("REF",ref)
      setborroweName(ref.userName)
      setborroweTel(ref.userTel)

    })*/
    /*
    deviceRef.child("deviceType/"+device).once('value',(snapshot) => {
      const ref = snapshot.val()
      console.log("scanned",ref)
      //borrowingList.push({"deviceType/"+device,...ref})
      if(ref != null){
        borrowingList.push({"ref":ref})
        borrowingPathList.push(device)
        
        setBorrowingList(borrowingList)
        setBorrowingPathList(borrowingPathList)
        for(let key in borrowingList){
          console.log("key >> ",borrowingList[key])
        }
        console.log(borrowingList)
      }
      
    })*/

  },[device])

  return (
    
    <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      <Card>
       
        <View style={{marginTop:0}}>
          <Text style={styles.headerText}>Borrower name</Text>
          <Text style={styles.contentText}>{borrowerName || "No currently borrower"}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Borrower phone number</Text>
          <Text style={styles.contentText}>{borrowerTel || "No currently borrower"}</Text>
        </View>
     </Card>
      
    <View style={{marginTop:16,flex:1}}>
    {returningDevice.map((Device,index) =>
        <Card>
          <View>
            <Text style={styles.headerText}>Device brand</Text>
          <Text style={styles.contentText} > {Device.deviceBrand || ''}</Text>
          <Text style={styles.headerText}>device Model</Text>
          <Text style={styles.contentText} > {Device.deviceModel || ''}</Text>
          <Text style={styles.headerText}>S/N</Text>
          <Text style={styles.contentText}>{Device.deviceSerialNumber || ''}</Text>
          </View>
        </Card>
    )}
    </View>
     
  
    <View>
      <Button disabled={!checkBorrower} 
      title="RETURN" 
      color="#2C9086" 
      onPress={()=> {
        AddReturnDeviceToHistory(device,borrowerUID,deviceData),
        ChangeCurrentStatusToReturnInDatabase(),
        Alert.alert("","Complete",[{
          text:'OK',
          onPress: () => navigation.goBack()
        }])
      }}/>
    </View>
      </View>
      
  )
}

export default ReturningForm


const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
})
