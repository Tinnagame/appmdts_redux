import React ,{useState,useEffect, useMemo} from "react";
import { View, Text, StyleSheet,TouchableOpacity,FlatList,RefreshControl} from "react-native";
import auth from '@react-native-firebase/auth'
import {db,userRef} from '../../../configs/firestore'
import { Button } from "native-base";
import DateTimePicker from '@react-native-community/datetimepicker'
import { Picker } from '@react-native-community/picker'
import moment from 'moment'
import {map,filter} from 'lodash'
import { Card,Icon } from 'react-native-elements'
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";


const AdminDeviceBorrowHistory = () => {
  const navigator = useNavigation();
    const user = auth().currentUser;
    const [historyList,setHistoryList] = useState([]);
    const [selectHistoryList,setSelectHistoryList] = useState([])
    const [selectedDate, setSelectedDate] = useState(moment().format('YYYY/MM/DD'))
    const [selectedYear, setSelectedYear] = useState('')
    const [date, setDate] = useState(Date.parse(moment().format('L')))
    const [mode, setMode] = useState('date')
    const [show, setShow] = useState(false)
    const [refreshing,setRefreshing] = useState(false)


    const onRefresh = () => {
      setRefreshing(true);

      wait(1000).then(() => setRefreshing(false));
    }

    const wait = (timeout) => {
      return new Promise(resolve => {
        setTimeout(resolve, timeout);
      });
    }
    useEffect(() => {
  console.log("-------------------UseEffect---------------------- ")
    console.log("selectedDate => ",selectedDate)
    //
    console.log(moment(date).format('YYYY/MM/DD'))
    userRef.once('value',(snapshot) => {
      console.log('ref ontime')
      const userSnap = snapshot.val()
      console.log("UserSnap",userSnap)
      const hist = []
      
      for(let key in userSnap) {
        console.log("key",key)
        console.log(key,userSnap[key].userTel)
      //  console.log(userSnap[key].role === "borrower")
        if(userSnap[key].userRole == "borrower"){
          //console.log("key",key)
        const HistoryRef = userRef.child(key).child('History/BorrowingHistory')
        .child(moment(date).format('YYYY/MM/DD'))
          HistoryRef.once('value',(snapshot) => {
            const history = snapshot.val()
            console.log("hitory Snap >>   ",history)
            for(let id in history) {
              userRef.child(id).once('value',(snapshot) => {
                const ref = snapshot.val();

                const PersonHistoryRef = HistoryRef.child(key);
                let borrowingDeviceAmount = 0;
                PersonHistoryRef.once('value', (snapshot) => {
                  const ref = snapshot.val();
                console.log("PHR >> ",ref)
                  for(let key in ref){
                    borrowingDeviceAmount++
                  }
                })
                
                
                //naming
                hist.push({id,userTel:userSnap[key].userTel,userName:ref.userName,borrowingDeviceAmount:borrowingDeviceAmount});

              })
              
            }

          })
         // setSelectHistoryList(hist)
          
        }
      }setSelectHistoryList(hist)
      //console.log(selectHistoryList)
    })
 },[date,refreshing]);

   const onChange = (event, selectingDate) => {
    const currentDate = selectingDate || date
    console.log("---------------------------------")
    //console.log("LOG current Date >> ",moment(currentDate).format('YYYY/MM/DD'))
    //console.log("LOG selecting Date >> ",moment(selectingDate).format('YYYY/MM/DD'))
    setShow(Platform.OS === 'ios')
    setDate(currentDate)
    setShow(false)
  }
   const showMode = (currentMode) => {
    setShow(true)
    setMode(currentMode)
  }
   const showDatepicker = () => {
    showMode('date')
  }
  
return(
    <View style={{flex:1, backgroundColor: '#f3f3f3'}}>
        <View style={{ backgroundColor: '#f3f3f3', flexDirection:'row'} }>
        <Text style={styles.sectionHeader}>device borrow history of </Text>
        {/*<Text style={styles.sectionHeader}>{setectedDate}</Text>*/}
        
        
        <TouchableOpacity 
                onPress={showDatepicker}>
                <View style={ styles.sectionHeader} >
                  <View style={{flexDirection:'row',}}>
                    <Text style={{fontSize:16}}>
                      {moment(date).format('DD/MM/YYYY')}
                    </Text>
                    {/*
                    <Icon name="tune" size={30} color={'black'} style={{margins:10}}
                    onPress={() => {{showDatepicker; console.log("icon click")}}}
                    />
                    */}
                  </View>
                  
                </View>
                
         </TouchableOpacity>
             
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  timeZoneOffsetInMinutes={0}
                  mode={mode}
                  is24Hour={true}
                  display="spinner"
                  onChange={onChange}
                />
              )}
        
        
        
        </View>

        <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}>
      
        
        {console.log(selectHistoryList)}
        {!!selectHistoryList.length &&
        selectHistoryList.map((Id, index) => 
          <TouchableOpacity
            key={Id.id}
            onPress={() => {navigator.navigate("PersonHistoryInformation",{
              UID: Id.id,
              Date:moment(date).format('YYYY/MM/DD')
            })}}
            >
            <Card key={Id.id} >
              <View
                style={styles.card
                
                }>
                <View>
                  <Text style={{ marginBottom: 8 }}>
                    {Id.userName} ( {Id.borrowingDeviceAmount}) 
                  </Text>
                  <View style={{ marginBottom: 8 ,flexDirection:'row'}}>
                    <Icon
              name="ios-call"
              type="ionicon"
              color="black"
              style={{fontSize:5,height:25,width:25}}
            />
            <Text style={{paddingLeft:10}} >
                  {Id.userTel}
                  </Text>
                  </View>
            
                  
                </View>
                
              </View>
            </Card>
          </TouchableOpacity>
        )}
        
        </ScrollView>
        {!selectHistoryList.length &&
         <Text style={{flex:1,alignSelf:'center'}}>There is no History on this day</Text>}
        
   </View>
 
    
            
    )

}

    



const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    
    
  },
  approved: {
    color: '#28CD1A'
  },
  rejected: {
    color: '#ff0000'
  },
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },
    sectionHeader: {
      color: 'grey',
      textTransform: 'uppercase',
      fontSize: 15,
      marginLeft: 16,
      marginTop: 16,
      marginRight: 16
    },
  })
export default AdminDeviceBorrowHistory