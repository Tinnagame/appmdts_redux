import { last } from 'lodash'
import React, { useState } from 'react'
import { View, Text, Button, ScrollView,Alert } from 'react-native'
import { Input } from 'react-native-elements'
import  auth from '@react-native-firebase/auth'
import {useNavigation} from '@react-navigation/native'
import { userRef } from '~/configs/firestore'
const RegisterPage = () => {
  const navigation = useNavigation()
  const [name, setName] = useState(' ')
  const [lastName, setLastName] = useState('')
  const [phoneNumber, setPhoneNumber] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const checkName = name != ''
  const checkLastName = lastName != ''
  const checkPhoneNumber = phoneNumber != ''
  const checkEmail = email !=''
  const checkConfirmPass = confirmPassword != ''
  const checkPassword = password != ''
  
  const setTel = (value) => {
    setPhoneNumber(value)
    return  phoneNumber.slice(0,3)+"-"+phoneNumber.slice(3,6)+"-"+phoneNumber.slice(6,10)
  }

  const Register = () => {
    let userTel = phoneNumber.slice(0,3)+"-"+phoneNumber.slice(3,6)+"-"+phoneNumber.slice(6,10)
    auth().createUserWithEmailAndPassword(email,password).then((user) =>{
      console.log(user.user)
      userRef.child(user.user.uid).update({
        userName:name+" "+lastName,
        userTel:userTel,
        userRole:"borrower"
      })
    })
    .catch((error) => {
    console.log(error)
    })
  }

  
  const validatePassword = () => {
      return (password === confirmPassword ? true : false)
  }

  const notEmptyInput = () => {
      return (name && lastName && phoneNumber && email && password 
        && confirmPassword !== '' ? console.log('correct'):console.log('empty input'))
  }
  console.log(phoneNumber.slice(0,3)+"-"+phoneNumber.slice(3,6)+"-"+phoneNumber.slice(6,10))
  return (
    <ScrollView style={{ flex: 1, borderWidth: 1 }}>
      {/* <Text style={{ flex: 0.5, borderWidth: 1 }}> This is register page </Text> */}
      <View
        style={{
          paddingBottom: 16,
          paddingLeft: 16,
          paddingRight: 16,
          paddingTop: 16,
        }}>

        <Input
          label="Name" 
          placeholder="Jiramed"
          onChangeText={(value) => setName(value)}
        />

        <Input
          label="Lastname"
          placeholder="Withunsappasiri"
          onChangeText={(value) => setLastName(value)}
        />
        <Input
          label="Phone Number"
          placeholder="080 123 4567"
          keyboardType='numeric'
          //textContentType="telephoneNumber"
          maxLength={10}
          
          //onChangeText={phoneNumber.slice(0,3)+"-"+phoneNumber.slice(3,6)+"-"+phoneNumber.slice(6,10)}
          onChangeText={(value) => setPhoneNumber(value)}
        />
        <Input
          label="Email"
          placeholder="myemail@mdts.com"
          textContentType="emailAddress"
          
          onChangeText={(value) => setEmail(value)}
        />
        <Input
          label="Password (atleast 6 character)"
          placeholder="••••••"
          secureTextEntry={true}
          maxLength={10}
          onChangeText={(value) => setPassword(value)}
        />
        <Input
          label="Confirm Password"
          placeholder="••••••"
          secureTextEntry={true}
          maxLength={10}
          onChangeText={(value) => setConfirmPassword(value)}
        />
        <Button
        disabled={!checkName || !checkLastName
        || !checkEmail || !checkPhoneNumber ||
        !checkConfirmPass || !checkPassword}
          color="#2C9086"
          title="register"
          onPress={() => {{
            if(validatePassword()){
              Register(),
              Alert.alert("","Register Complete",[{
                
                text:'OK',
                onPress: () => navigation.goBack()
              }])
            } else if (validatePassword() == false) {
              Alert.alert("Error","Password not match",[{
                text:'OK'
              }])
            }
            
             notEmptyInput()}}}
        />
      </View>
    </ScrollView>
  )
}

export default RegisterPage
