import React , {useEffect,useState}from 'react'
import { ScrollView, Text,StyleSheet,Button, Alert,View} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { userRef ,locationRef, deviceRef} from '~/configs/firestore'
import { Card,Input,  } from 'react-native-elements'
import { TextInput } from 'react-native-gesture-handler'
import  auth  from '@react-native-firebase/auth'
import {INIT_USER} from '../../../Functions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import dayjs from 'dayjs'

const Maintenance = (props) => {
const navigation = useNavigation()
const {devicePath,deviceBrand,deviceModel,deviceSerialNumber,currentStatus} = props.route.params
const [userName,setUserName] = useState('')
const [userTel,setUserTel] = useState('')
const [Problem,setProblem] = useState('')
const [MaintenanceNo1,setMaintenanceNo1] = useState('')
const [MaintenanceNo2,setMaintenanceNo2] = useState('')
const [MaintainerName,setMaintainerName] = useState('')
const [device,setDevice] = useState([])
const SendMaintenace = () => {
  let MaintenanceNo = MaintenanceNo1+"-"+MaintenanceNo2
  deviceRef.child("deviceType/"+devicePath).update({
  
    requestorName:userName,
    MaintenanceNo:MaintenanceNo,
    Problem:Problem,
    MaintainerName:MaintainerName,
    currentStatus:"Maintaining",
    maintenanceDate:dayjs().format("YYYY-MM-DD")
  })
}
/*
const changeStatusToMaintenance = () => {
deviceRef.child(devicePath).update({deviceStatus:"Maintained"})
}
*/


/*6312 0001*/
const fetchDevice = () => {
  deviceRef.child(devicePath).once('value',(snapshot) => {
    console.log("SNAP",snapshot.val())
    setDevice(snapshot.val())
  })
}

const retrieveDevice = () => {
  deviceRef.child("deviceType/"+devicePath).update({
    currentStatus:"Returned"})
}
useEffect(() => {
console.log(devicePath)
INIT_USER(auth().currentUser.uid,setUserName,setUserTel)
fetchDevice()
},[])

const renderForm = () => {
  if(currentStatus == "Maintained") {
    return <RetrieveForm/>
  }
  else {
    return <MaintainForm/>
  }
}
const MaintainForm = () => {
  return (
    
    <KeyboardAwareScrollView style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      <Card>
        <View><Text>Maintain form</Text></View>
        <View style={{marginTop:0}}>
          <Text style={styles.headerText}>Device Brand</Text>
          <Text style={styles.contentText}>{deviceBrand || "-"}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Model</Text>
          <Text style={styles.contentText}>{deviceModel || "-"}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>S/N</Text>
          <Text style={styles.contentText}>{deviceSerialNumber || "-"}</Text>
        </View>
     </Card>

     <Card>
       
        <View style={{marginTop:0}}>
          <Text style={styles.headerText}>Requestor Name</Text>
          <Text>{userName}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Maintenace Form No.</Text>
          <View style={{flexDirection:'row'}}>
          <TextInput style={styles.textInputBox}
          placeholder={"0000"}
           value={MaintenanceNo1}
           maxLength={4}
           onChangeText={(text) => setMaintenanceNo1(text)}/>
          <Text style={{paddingTop:13}}>   -   </Text>
          <TextInput style={styles.textInputBox}
          placeholder={"0000"}
           value={MaintenanceNo2}
           maxLength={4}
           onChangeText={text => setMaintenanceNo2(text)}/>
          
          </View>
          </View>
          <View style={styles.textGap}>
          <Text style={styles.headerText}>Problem (500 characters limit)</Text>
          <TextInput style={styles.textInputBox}
          value={Problem}
          scrollEnabled={true}
          maxLength={500}
          multiline={true}
          numberOfLines={5}
          onChangetext={(value) => setProblem(value)}
          
          />
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Order Taken By</Text>
          <TextInput style={styles.textInputLine} 
          placeholder={"Fill Maintainer Name"}
          value={MaintainerName}
          onChangeText={(value) => setMaintainerName(value)}></TextInput>
        </View>
     </Card>
     <View style={{width:300, paddingTop:20,alignSelf:'center'}}>
       <Button title='SEND FOR MAINTENANCE'
       color="#2C9086"
       onPress={() => SendMaintenace()}></Button>
     </View>
      
    
     </KeyboardAwareScrollView>
)
}

const RetrieveForm = () => {
  return (
    
    <ScrollView style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      
      <Card>
      <View><Text>retrieve form</Text></View>
        <View style={{marginTop:0}}>
          <Text style={styles.headerText}>Device Brand</Text>
          <Text style={styles.contentText}>{deviceBrand || "-"}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Model</Text>
          <Text style={styles.contentText}>{deviceModel || "-"}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>S/N</Text>
          <Text style={styles.contentText}>{deviceSerialNumber || "-"}</Text>
        </View>
     </Card>

     <Card>
       
        <View style={{marginTop:0}}>
          <Text style={styles.headerText}>Requestor Name</Text>
          <Text>{requestorName}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Maintenace Form No.</Text>
          <View style={{flexDirection:'row'}}>
          {/*<TextInput style={styles.textInputBox}
          placeholder={"0000"}
           value={MaintenanceNo1}
           enabled={false}/>
          <Text style={{paddingTop:13}}>   -   </Text>
          <TextInput style={styles.textInputBox}
          placeholder={"0000"}
           value={MaintenanceNo2}
           enabled={false}/>
  */}
          </View>
          </View>
          <View style={styles.textGap}>
          <Text style={styles.headerText}>Problem (500 characters limit)</Text>
          {/*<TextInput style={styles.textInputBox}
          value={Problem}
          scrollEnabled={true}
          maxLength={500}
          multiline={true}
          numberOfLines={5}
          enabled={false}
/>*/}
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Order Taken By</Text>
         {/* <TextInput style={styles.textInputLine} 
          placeholder={"Fill Maintainer Name"}
          value={MaintainerName}
enabled={false}></TextInput>*/}
        </View>
     </Card>
     <View style={{width:300, paddingTop:20,alignSelf:'center'}}>
       <Button title='SEND FOR MAINTENANCE'
       color="#2C9086"
       onPress={() => retrieveDevice}></Button>
     </View>
      
    
     </ScrollView>
)
}
  return (
    renderForm()
  )
  
}


export default Maintenance

const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
  textInputBox:{
    borderRadius:8,
    borderColor:'gray',
    borderWidth:1,
  },
  textInputLine:{
    borderBottomColor:'black',
    borderBottomWidth:1
  }

})
