import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Button,
  ScrollView,
} from 'react-native'

import { Card, Icon } from 'react-native-elements'
import auth from '@react-native-firebase/auth'
import { useNavigation } from '@react-navigation/native'
import { map, filter, concat } from 'lodash'
import { db, borrowerRef, userRef, deviceRef } from '../../../configs/firestore'

//temp

const ViewSelectTypeDevice = (props) => {
  const navigation = useNavigation()
  const { type } = props.route.params
  //const {currentUser}

  // const {uid} = route.params
  //console.log(route)
  const [userRole,setUserRole] = useState('')
  const [DeviceTypeList, setDeviceTypeList] = useState([])
  const [DeviceList, setDeviceList] = useState([])
  //
  const setFontColor = (status) => {
    if (status == "In-use")
    return "orange"
    if (status == "Returned")
    return "#1B9A18"
    if(status == null)
    return "#1B9A18"
    if(status == "Maintained")
    return "#666666"
  }
  const Init_UserRole =() => {
    userRef.child(auth().currentUser.uid).once('value',(snapshot) => {
      setUserRole(snapshot.val().userRole)
    })
  }
  useEffect(() => {
    Init_UserRole()
    // console.log(DeviceTypeList)
    deviceRef.child('deviceType/' + type).on('value', (snapshot) => {
      const devices = snapshot.val()
      // console.log('device', devices)
      const deviceList = []
      const TypeList = []
      for (let key in devices) {
        // console.log('key', key)
        // TypeList.push({type,type})
        //console.log(TypeList)
        //deviceList.push({type:devices[type] });
        deviceList.push({ key, ...devices[key] })
      }
      // console.log('deviceList >>', deviceList)
      setDeviceList(deviceList)
      //setDeviceTypeList(TypeList)
    })
    /*/
      for(let key in DeviceTypeList) {
        console.log("DTL >>>",DeviceTypeList[key].type)
        
    
      }*/
  }, [])

  return (
    <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
      {/*Request*/}
      <Text
        style={{
          marginTop: 16,
          marginLeft: 16,
          textTransform: 'uppercase',
          fontSize: 16,
          color: 'black',
        }}>
        device list
      </Text>
      {/* card*/}

      {/*try - mapped */}

      <ScrollView contentContainerStyle={{ paddingBottom: 32 }}>
        {!!DeviceList.length &&
          DeviceList.map((Device, index) => (
            console.log(Device),
            <Card
              key={type + '/' + Device.key}
              containerStyle={{
                borderRadius: 8,
                borderWidth: 0,
                elevation: 8,
              }}>
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('DeviceInformationPage', {
                    userRole: userRole,
                    type: type,
                    key: type + '/' + Device.key,
                    ...Device,
                  })
                }>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginBottom: 8, color: '#757575' }}>
                        Brand: {''}
                      </Text>
                      <Text style={{ marginBottom: 8 }}>
                        {Device.deviceBrand}
                      </Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginBottom: 8, color: '#757575' }}>
                        Model : {''}
                      </Text>
                      <Text style={{ marginBottom: 8 }}>
                        {Device.deviceModel}
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginBottom: 8, color: '#757575' }}>
                        S/N : {''}
                      </Text>
                      <Text style={{ marginBottom: 8 }}>
                        {Device.deviceSerialNumber}
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ marginBottom: 8, color: '#757575' }}>
                        Location: {''}
                      </Text>
                      <Text style={{ marginBottom: 8 }}>
                        {Device.currentLocation
                          ? Device.currentLocation
                          : 'Blood Gas Room'}
                      </Text>
                    </View>
                    
                </View>
                <View style={{flexDirection:"row"}}>
                      <Text style={{color:setFontColor(Device.currentStatus)}}>{Device.currentStatus ? Device.currentStatus : 'Returned'}</Text></View>
                  </View>
              </TouchableOpacity>
            </Card>
          ))}
      </ScrollView>
      {/*ACTION BUTTON*/}
    </View>
  )
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  Returned: {
    color:'#1B9A18'
  },
  sectionHeader: {
    color: 'grey',
    textTransform: 'uppercase',
    marginLeft: 16,
    marginTop: 16,
  },
})

export default ViewSelectTypeDevice
