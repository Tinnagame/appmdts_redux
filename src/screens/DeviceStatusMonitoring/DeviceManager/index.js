
import { useNavigation } from '@react-navigation/native'
import React, { useState } from 'react'
import { View, Text, StyleSheet, Modal, Alert, ScrollView,Button } from 'react-native'
import {Card} from 'react-native-elements'
import { TouchableOpacity } from 'react-native-gesture-handler'

const DeviceManager = () => {
    const navigation = useNavigation()
    return (
        
        <View style={{flex:1,paddingTop:250}}>
        
        <TouchableOpacity onPress={() => {navigation.navigate("AddDevice")}}>
        <Card><Text style={{alignSelf:'center'}}>ADD DEVICE</Text></Card>
        </TouchableOpacity>
        

        <TouchableOpacity style={{paddingTop:20}} onPress={() => {navigation.navigate("RemoveDeviceQRcodeScanner",{device:null})}}>
        <Card><Text style={{alignSelf:'center'}}>REMOVE DEVICE</Text></Card>
        </TouchableOpacity>
        

        </View>
    )

}
export default DeviceManager