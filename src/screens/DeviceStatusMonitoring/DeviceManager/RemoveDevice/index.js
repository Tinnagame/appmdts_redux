
import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Modal, Alert, ScrollView,Button} from 'react-native'
import dayjs from 'dayjs'
import { Card,Input,  } from 'react-native-elements'
import { INIT_LOCATION } from '~/Functions'
import { Picker } from '@react-native-community/picker'
import {deviceRef} from '../../../../configs/firestore'
const RemoveDevice = (props) => {
    const navigation = useNavigation()
    const [deviceBrand,setDeviceBrand] = useState('')
    const [deviceModel,setDeviceModel] = useState('')
    const [deviceSerialNumber,setDeviceSerialNumber] = useState('')
    const [registeredDate,setRegistedDate] = useState([])
    const [selectedLocation,setSelectedLocation] = useState('')
    const [deviceType,setDeviceType] = useState('')
    
    const {device} = props.route.params

    const fetchDevice = () => {
      if(device)
      deviceRef.child("deviceType/"+device).once('value', (snapshot) => {
        console.log(snapshot.val())
        setDeviceType(device.split('/')[0])
        setDeviceBrand(snapshot.val().deviceBrand)
        setDeviceModel(snapshot.val().deviceModel)
        setDeviceSerialNumber(snapshot.val().deviceSerialNumber)
        setRegistedDate(snapshot.val().registeredDate)
      })
    }
  useEffect(() => {
    fetchDevice()
    
  },[])

    const RemoveDeviceFromDatabase = () => {
        const TypeRef = firebase.database().ref("Devices/deviceType").child(device).remove().then(Alert.alert(
          "","Medical Device Removed",[{
            text:'OK',onPress:() => navigation.navigate("Home")
          }]
        ))
    
    }
    return (
        
        <View style={{flex:1}}>
        <Card>
          
            
            <View style={{}}>
              <Text style={styles.titleText}>Device Type</Text>
              <Text style={styles.itemText}>{deviceType}</Text>
              <Text style={styles.titleText}>Brand</Text>
              <Text style={styles.itemText}>{deviceBrand}</Text>
              <Text style={styles.titleText}>Model</Text>
              <Text style={styles.itemText}>{deviceModel}</Text>
              <Text style={styles.titleText}>S/N</Text>
              <Text style={styles.itemText}>{deviceSerialNumber}</Text>
              <Text style={styles.titleText}>Registered on</Text>
              <Text style={styles.itemText}>{registeredDate}</Text>
        </View>
        

            

            
        </Card> 
        <View style={{width:300, paddingTop:25,alignSelf:'center'}}>
        <Button title="REMOVE DEVICE" color="#2C9086" onPress={ () => Alert.alert("",
        "Are you sure to remove this medical device",[
          {text:'CANCEL'},{text:'OK',onPress:() => {
            RemoveDeviceFromDatabase()
          }}
        ])}/>
        </View> 
        
        </View>
    )

}
export default RemoveDevice

const styles = StyleSheet.create({
  pickerLabel: {
    color: 'grey',
  },
  titleText: {
    lineHeight:24,
    fontSize:14,
    color:'#757575'
  },
  itemText: {
    lineHeight:20,fontSize:16,color:'#000000',
    paddingLeft:5,paddingBottom:10
  }


})
