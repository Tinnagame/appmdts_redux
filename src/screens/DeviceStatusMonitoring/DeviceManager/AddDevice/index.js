
import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Modal, Alert, ScrollView,Button} from 'react-native'
import dayjs from 'dayjs'
import { Card,Input,  } from 'react-native-elements'
import { INIT_LOCATION } from '~/Functions'
import { values } from 'lodash'
import { Picker } from '@react-native-community/picker'
import { deviceRef } from '~/configs/firestore'

const AddDevice = () => {
    const navigation = useNavigation()
    const [deviceBrand,setDeviceBrand] = useState('')
    const [deviceModel,setDeviceModel] = useState('')
    const [deviceSerialNumber,setDeviceSerialNumber] = useState('')
    const [typeList,setTypeList] = useState([])
    const [selectedType,setSelectedType] = useState('')
  
    const fetchType = () => {
      const arr = []
      deviceRef.child('deviceType').once("value",(snapshot) => {
        for(let type in snapshot.val()) {
          arr.push({name:type})
        }
      })
      setTypeList(arr)
    }
    useEffect(() => {
    fetchType()
    console.log(typeList)
  },[])

    const AddDeviceToDatabase = () => {
        const TypeRef = firebase.database().ref("Devices/deviceType").child(selectedType)
    TypeRef.push({
      deviceBrand:deviceBrand,
      deviceModel:deviceModel,
      deviceSerialNumber:deviceSerialNumber,
      registeredDate:dayjs(Date()).format('YYYY-MM-DD'),
      currentStatus:"Returned",
      currentLocation:"Blood Gas Room",
    })
    Alert.alert(
      "Complete","Adding Complete"
    )
    }
    return (
        
        <View style={{flex:1}}>
        <Card>
        <View style={{ marginTop: 16 }}>
              <Text style={styles.pickerLabel}> Type </Text>
              {<Picker
                style={{ height: 50, width: '100%' }}
                mode="dropdown"
                selectedValue={selectedType}
                onValueChange={(itemValue) => {setSelectedType(itemValue)}}>


                <Picker.Item label="select type" value="0" />
                {typeList ? typeList.map((type, index) => 
              
                <Picker.Item label={type.name} value={type.name} key={index}/> ) : '' }
              
                </Picker> }
            </View>

            <View>
              <Input
              label="Brand"
              placeholder="Input device Brand"
              onChangeText={(value) => setDeviceBrand(value)}
              />

              <Input
              label="Model"
              placeholder="Input device Model"
              onChangeText={(value) => setDeviceModel(value)}
              />
              <Input
              label="S/N"
              placeholder="Input device SerialNumber"
              onChangeText={(value) => setDeviceSerialNumber(value)}
              />
            </View>

            <View>
              <Text style={{color:'red'}}>You will not able to edit medical device information</Text>
              <Text style={{color:'red',alignSelf:'center'}}>after added to the system.</Text>
            </View>
        </Card> 
        <View style={{width:300, paddingTop:25,alignSelf:'center'}}>
        <Button title="ADD DEVICE" color="#2C9086" onPress={ () => Alert.alert("",
        "Please make sure that you input the correct medical device information",[
          {text:'CANCEL'},{text:'OK',onPress:() => {
            AddDeviceToDatabase(),Alert.alert('','New Medical Device Added',[{
              text:'OK',onPress:() => {
                navigation.goBack()
              }
            }])
          }}
        ])}/>
        </View> 
        
        </View>
    )

}
export default AddDevice

const styles = StyleSheet.create({
  pickerLabel: {
    color: 'grey',
  }})