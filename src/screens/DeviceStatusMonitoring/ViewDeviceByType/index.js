import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import { Card, Icon } from 'react-native-elements'

import { useNavigation } from '@react-navigation/native'
import { deviceRef } from '../../../configs/firestore'

import { ScrollView } from 'react-native-gesture-handler'

// import { Input } from 'native-base'
//temp

const ViewDeviceByType = ({}) => {
  const navigation = useNavigation()
  //const {currentUser}

  // const {uid} = route.params
  //console.log(route)

  const [DeviceTypeList, setDeviceTypeList] = useState([])
  const [, setDeviceList] = useState([])
  const [search, setSearch] = useState('')
  const [searchContent, setSearchContent] = useState([])
  //

  /*
  const updateSearch = (search) => {
    setSearch(search)
    filterSearch(search)
  }*/
  /*
  const filterSearch = (searchText) => {
    /*deviceRef.child("deviceType/"+searchText).once('value', (snapshot) => {
      const searchDevice = snapshot.val();
      console.log("SD >> ",searchDevice)
    })*/

  /*deviceRef.child("deviceType").orderByChild(searchText).startAt(searchText).endAt(searchText+"\uf8ff")
    .once('value',(snapshot) => {
      const searchDevice = snapshot.val()
      console.log(searchText,">>",searchDevice)
    })
    if(searchText) {
      console.log("------------------------")
      deviceRef.child('deviceType/').orderByKey().startAt(searchText).on('value',(snapshot) => {
       // console.log(searchText,">>",snapshot.val())
        const contentRef = snapshot.val()
        for(let key in contentRef) {
          console.log(key, " >>> ", contentRef[key])
        
        }
        //console.log("path >> ",deviceRef.child('deviceType/').orderByKey().startAt(searchText))
      })
    }
  }*/
  const fetchData = () => {
    deviceRef.child('deviceType').on('value', (snapshot) => {
      const devices = snapshot.val()
      console.log('device', devices)
      const deviceList = []
      const TypeList = []
      for (let type in devices) {
        //counting amount
        deviceRef.child('deviceType/' + type).on('value', (snapshot) => {
          const temp = snapshot.val()
          console.log('TEMP >>>> ', temp)
          let tempNum = 0
          for (let key in temp) {
            tempNum++
          }
          TypeList.push({ type, type, amount: tempNum })
          deviceList.push({ type: devices[type], amount: tempNum })
        })
      }
      setDeviceList(deviceList)
      setDeviceTypeList(TypeList)
    })
  }
  useEffect(() => {
    fetchData()
  }, [])

  return (
    <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
      {/*Request*/}
      <Text
        style={{
          marginTop: 16,
          marginLeft: 16,
          textTransform: 'uppercase',
          fontSize: 16,
          color: 'black'
        }}>
        device type list
      </Text>
      <ScrollView contentContainerStyle={{ paddingBottom: 32 }}>
        {!!DeviceTypeList.length &&
          DeviceTypeList.map((Device) => (
            <TouchableOpacity
              key={Device.type}
              onPress={() =>
                navigation.navigate('DeviceByTypePage', {
                  type: Device.type,
                  //"1",id:'id',model:"model",amount:'amount',location:'location',data:'date'
                })
              }>
              <Card
                containerStyle={{
                  borderWidth: 0,
                  borderRadius: 10,
                  elevation: 8,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{ marginBottom: 8, fontSize: 16 }}>
                      {Device.type} ({Device.amount})
                    </Text>
                  </View>
                  <View>
                    <Icon
                      name="chevron-forward-outline"
                      type="ionicon"
                      color="black"
                    />
                  </View>
                </View>
              </Card>
            </TouchableOpacity>
          ))}
      </ScrollView>
      {/*ACTION BUTTON*/}
    </View>
  )
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  sectionHeader: {
    color: 'grey',
    textTransform: 'uppercase',
    marginLeft: 16,
    marginTop: 16,
  },
})

export default ViewDeviceByType
