import React, { useState } from 'react'
import { View, Text, StyleSheet, Modal, Alert, ScrollView } from 'react-native'

import { Card, Button, withTheme } from 'react-native-elements'

import { useNavigation } from '@react-navigation/native'

import QRCode from 'react-native-qrcode-svg'
import GestureRecognizer from 'react-native-swipe-gestures'
import { upperCase, wrap } from 'lodash'
import { Right } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler'

const DeviceInformation = (props) => {
  const navigator = useNavigation()
  const {
    type,
    key,
    deviceBrand,
    deviceSerialNumber,
    deviceModel,
    currentStatus,
    currentLocation,
    returningTime,
    borrowingDate,
    registeredDate,
    nextCalibrationTime,
    nextPreventingTime,
    borrowerName,
    borrowerTel,
  } = props.route.params

  //use to change tab underline color
  const [isInvisible, setIsInvisible] = useState(true)
  const [leftLineColor, setLeftLineColor] = useState('#2C9086')
  const [rightLineColor, setRightLineColor] = useState('white')

  const swiftLeft = () => {
    setLeftLineColor('#2C9086')
    setRightLineColor('white')
    setIsInvisible(true)
  }
  const swiftRight = () => {
    setLeftLineColor('white')
    setRightLineColor('#2C9086')
    setIsInvisible(false)
  }

  return (
    <ScrollView style={{ flex: 1, backgroundColor: '#ffffff' }}>
      <GestureRecognizer
        onSwipeLeft={() => swiftLeft()}
        onSwipeRight={() => swiftRight()}>
        <Card
          containerStyle={{
            flex: 1,
            borderBottomWidth: 0,
            borderRadius: 10,
            marginBottom: 16,
          }}>
          <View
            style={{
              justifyContent: 'center',
              marginLeft: '25%',
              marginBottom: '8%',
            }}>
            <Text>{key}</Text>
            <QRCode value={key} size={170}  />
          </View>

          {/* tab below a QR code */}
          <View
            style={{
              flex: 0.2,
              flexDirection: 'row',
            }}>
            <View
              style={{
                flex: 0.5,
                borderBottomWidth: 3,
                borderBottomColor: leftLineColor,
              }}>
              {/* <Button
                title="DEVICE INFO"
                type="clear"
                onPress={() => swiftLeft()}
                titleProps={{ color: 'black' }}
              /> */}
              <TouchableOpacity onPress={() => swiftLeft()}>
                <Text
                  style={{
                    textTransform: 'uppercase',
                    alignSelf: 'center',
                    paddingTop: 8,
                    paddingBottom: 8,
                  }}>
                  device info
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 0.5,
                borderBottomWidth: 3,
                borderBottomColor: rightLineColor,
              }}>
              {/* <Button
                title="BORROWER INFO"
                type="clear"
                onPress={() => swiftRight()}
                titleProps={{ color: 'black' }}
              /> */}
              <TouchableOpacity onPress={() => swiftRight()}>
                <Text
                  style={{
                    textTransform: 'uppercase',
                    alignSelf: 'center',
                    paddingTop: 8,
                    paddingBottom: 8,
                  }}>
                  borrowing info
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ flex: 0.8 }}>
            <Text>
              {' '}
              {isInvisible ? (
                <View>
                  {/* <Text style={styles.textGap}> key: {key} </Text> */}
                  <View style={{ marginTop: 8 }}>
                    <Text style={styles.headerText}>Device Brand</Text>
                    <Text style={styles.contentText}>{deviceBrand || ''}</Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>S/N</Text>
                    <Text style={styles.contentText}>
                      {deviceSerialNumber || ''}
                    </Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Model</Text>
                    <Text style={styles.contentText}>{deviceModel || '-'}</Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Current status</Text>
                    <Text style={styles.contentText}>
                      {currentStatus || '-'}
                    </Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Current Location</Text>
                    <Text style={styles.contentText}>
                      {currentLocation || '-'}
                    </Text>
                  </View>
                </View>
              ) : (
                <View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Registered on</Text>
                    <Text style={styles.contentText}>
                      {registeredDate || '-'}
                    </Text>
                  </View>
                  {/* <View style={styles.textGap}>
                    <Text style={styles.headerText}>Next preventing test</Text>
                    <Text style={styles.contentText}>
                      {nextPreventingTime || '-'}
                    </Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Next calibration Test</Text>
                    <Text style={styles.contentText}>
                      {nextCalibrationTime || '-'}
                    </Text>
                  </View> */}
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Last borrower name</Text>
                    <Text style={styles.contentText}>
                      {borrowerName || '-'}
                    </Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>
                      Last borrower phone number
                    </Text>
                    <Text style={styles.contentText}>{borrowerTel || '-'}</Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Last Returning time</Text>
                    <Text style={styles.contentText}>
                      {returningTime || '-'}
                    </Text>
                  </View>
                  <View style={styles.textGap}>
                    <Text style={styles.headerText}>Last Borrowing Date</Text>
                    <Text style={styles.contentText}>
                      {borrowingDate || '-'}
                    </Text>
                  </View>
                </View>
              )}
            </Text>
          </View>
        </Card>
      </GestureRecognizer>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
})

export default DeviceInformation
