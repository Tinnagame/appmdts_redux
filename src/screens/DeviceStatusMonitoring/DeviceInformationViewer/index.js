import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Modal, Alert} from 'react-native'

import { Card, Button } from 'react-native-elements'

import { useNavigation } from '@react-navigation/native'

import moment from 'moment'
import QRCode from 'react-native-qrcode-svg'
import { deviceRef } from '~/configs/firestore'
import { ScrollView } from 'react-native-gesture-handler'



const DeviceInformationViewer = (props) => {
 const navigator = useNavigation()
 const {device} = props.route.params
 const [thisDevice,setThisDevice] = useState([])

 const fetchingData =  () => {
  deviceRef.child('deviceType/'+device).once('value',(snapshot) => {
    //console.log(" Device log >>",snapshot.val())
    setThisDevice(snapshot.val())
    
  })
}
useEffect(()=> {
  fetchingData()
},[])
 
return(
<View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
  <ScrollView>
  <Card>
        <View style={{justifyContent:'center',marginLeft:"25%",marginBottom:'15%'}}>
          <QRCode
          value={device}
          size={170}
          />
        </View>
<View></View>
        <View>
          <Text style={styles.headerText}>Device Brand</Text>
          <Text style={styles.contentText}>{ thisDevice.deviceBrand || '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>S/N</Text>
          <Text style={styles.contentText}>{thisDevice.deviceSerialNumber || '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Model</Text>
          <Text style={styles.contentText}>{thisDevice.deviceModel|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Current status</Text>
          <Text style={styles.contentText}>{thisDevice.currentStatus|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Current location</Text>
          <Text style={styles.contentText}>{thisDevice.currentLocation|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Last Returning time</Text>
          <Text style={styles.contentText}>{thisDevice.returningTime|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Last Borrowing Date</Text>
          <Text style={styles.contentText}>{thisDevice.borrowingDate|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Registered on</Text>
          <Text style={styles.contentText}>{thisDevice.registeredDate|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Next preventing test</Text>
          <Text style={styles.contentText}>{thisDevice.nextPreventingTime|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Next calibration Test</Text>
          <Text style={styles.contentText}>{thisDevice.nextCalibrationTime|| '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Last borrower name</Text>
          <Text style={styles.contentText}>{thisDevice.borrowerName || '-'}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Last borrower phone number</Text>
          <Text style={styles.contentText}>{thisDevice.borrowerTel || '-'}</Text>
        </View>

      </Card>
      

  </ScrollView>
  </View>
)}
const styles = StyleSheet.create({
    textGap: {
      marginTop: 24,
    },
    headerText: {
      color: 'gray',
    },
    contentText: {
      marginLeft: 8,
      marginTop: 8,
    },
  })
  

export default DeviceInformationViewer