import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Button } from 'react-native-elements'
import { userRef } from '../../configs/firestore'
import DateTimePicker from '@react-native-community/datetimepicker'
import { VictoryPie } from 'victory-native'
//import * as dayjs from 'dayjs'
import {
  SlideBarChart,
} from 'react-native-slide-charts'

import { parseInt } from 'lodash'
import { split } from 'lodash'

const DashBoard = () => {
  const dayjs = require('dayjs') // or import dayjs from 'dayjs'
  const [show, setShow] = useState(false)
  const [mode, setMode] = useState('date')
  const [DashBoardData, setDashBoardData] = useState([])
  const [DeviceDashBoard, setDeviceDashBoard] = useState([])
  const [selectedDashBoardMode, setSelectedDashBoardMode] = useState('YEAR')
  const [selectedDate, setSelectedDate] = useState(parseInt(dayjs(Date())))
  const [markerSpacing, setMarkerSpacing] = useState(0)



  //month
  let Jan = 0
  let Feb = 0
  let Mar = 0
  let Apr = 0
  let May = 0
  let June = 0
  let July = 0
  let Aug = 0
  let Sept = 0
  let Oct = 0
  let Nov = 0
  let Dec = 0
  //week
  let Sun = 0
  let Mon = 0
  let Tues = 0
  let Wed = 0
  let Thu = 0
  let Fri = 0
  let Sat = 0
  //week value
  let SunVal = 0
  let MonVal = 0
  let TuesVal = 0
  let WedVal = 0
  let ThuVal = 0
  let FriVal = 0
  let SatVal = 0
  //DeviceType
  let AnesMachine = 0
  let Defibrillator = 0
  let FiberOptic = 0
  let PulseOximeter = 0
  let TourniquetSystem = 0
  let Ultrasound = 0
  let VideoLaryngoScope = 0
  let VitalSignMonitor = 0
  //day
  let first = 0
  let second = 0
  let third = 0
  let fourth = 0
  let fifth = 0
  let sixth = 0
  let seventh = 0
  let eighth = 0
  let ninth = 0
  let tenth = 0
  let eleventh = 0
  let twelveth = 0
  let thirtheenth = 0
  let fourteenth = 0
  let fifteenth = 0
  let sixteenth = 0
  let seventeenth = 0
  let eighteenth = 0
  let nineteenth = 0
  let twentieth = 0
  let twentyFirst = 0
  let twentySecond = 0
  let twentyThird = 0
  let twentyForth = 0
  let twentyFifth = 0
  let twentySixth = 0
  let twentySeventh = 0
  let twentyEighth = 0
  let twentyNinth = 0
  let thirtieth = 0
  let thirtieFirst = 0


  const renderX = (type) => {
    if(type == "WEEK") {
      return [{"x":"Sun"},{"x":"Mon"},{"x":"Tue"},{"x":"Wed"},{"x":"Thu"},{"x":"Fri"},{"x":"Sat"}]
    }
    if(type == "YEAR") {
      return [{"x":"Jan"},{"x":"Feb"},{"x":"Mar"},{"x":"Apr"},{"x":'May'},{"x":"Jun"},{"x":"Jul"},{"x":"Aug"},{"x":"Sep"},{"x":"Oct"},{"x":"Nov"},{"x":"Dec"}]
    }
    if(type == "MONTH") {
      return DashBoardData
    }
  }
  const colorPallete = [
    '#F67676', //pink
    '#55DCB4', //green
    '#F6A962', //orange
    '#62AFF6', //blue
    '#B562F6', //purple
    '#F6E862', //yellow
    '#BDB5B1', //grey
    '#34251D', //dark
  ]
  const resetDashBoard = () => {
    AnesMachine = null
    Defibrillator = null
    FiberOptic = null
    PulseOximeter = null
    TourniquetSystem = null
    Ultrasound = null
    VideoLaryngoScope = null
    VitalSignMonitor = null
    SunVal = null
    MonVal = null
    TuesVal = null
    WedVal = null
    ThuVal = null
    FriVal = null
    SatVal = null
  }

  //const DeviceData =[{x:"Hi",y:1}]
  const FetchYearDashBoard = () => {
    const testdashboardData = []
    resetDashBoard()
    console.log('----------FETCH-YEAR-DASHBOARD--------')
    const data = []
    userRef.once('value', (snapshot) => {
      const userSnap = snapshot.val()
      const hist = []
      let Year = []
      //let thisMonth = "0"
      let count = 0
      for (let key in userSnap) {
        if (userSnap[key].userRole == 'borrower') {
          const HistoryRef = userRef
            .child(key)
            .child('History/BorrowingHistory')
            .child(dayjs(selectedDate).format('YYYY'))
          HistoryRef.orderByKey().once('value', (snapshot) => {
            console.log('----------------')
            const year = snapshot.val()
            //console.log("HISTORY SNAP",year)
            for (let month in year) {
              console.log('LOOP month in Year')
              console.log('this is month ', month)
              //console.log("month >> ",month,' : ',year[month])
              for (let day in year[month]) {
                for (let person in year[month][day]) {
                  for (let device in year[month][day][person]) {
                    count++
                    let deviceType = split(
                      year[month][day][person][device]['UID'],
                      '/',
                    )[0]
                    hist.push({ [month]: { device, deviceType } })
                    console.log('LOG >>>>>', deviceType)
                  }
                }
              }
            }
          })
        }
      }
      //console.log(hist)
      //map data to year chart
      hist.map((item) => {
        for (let key in item) {
          switch (key) {
            case '01':
              Jan++
              break
            case '02':
              Feb++
              break
            case '03':
              Mar++
              break
            case '04':
              Apr++
              break
            case '05':
              May++
              break
            case '06':
              June++
              break
            case '07':
              July++
              break
            case '08':
              Aug++
              break
            case '09':
              Sept++
              break
            case '10':
              Oct++
              break
            case '11':
              Nov++
              break
            case '12':
              Dec++
              break
          }
        }
      })
      //
      Year.push(
        { x: 1, y: Jan },
        { x: 2, y: Feb },
        { x: 3, y: Mar },
        { x: 4, y: Apr },
        { x: 5, y: May },
        { x: 6, y: June },
        { x: 7, y: July },
        { x: 8, y: Aug },
        { x: 9, y: Sept },
        { x: 10, y: Oct },
        { x: 11, y: Nov },
        { x: 12, y: Dec },
      )
      const deviceList = []
      hist.map((item) => {
        for (let key in item) {
          //console.log("LOOOOOOP",item[key].deviceType)
          switch (item[key].deviceType) {
            case 'Anes Machine':
              AnesMachine++
              break
            case 'Defibrillator':
              Defibrillator++
              break
            case 'Fiber Optic':
              FiberOptic++
              break
            case 'Pulse oximeter':
              PulseOximeter++
              break
            case 'Tourniquet System':
              TourniquetSystem++
              break
            case 'Ultrasound':
              Ultrasound++
              break
            case 'Video LaryngoScope':
              VideoLaryngoScope++
              break
            case 'Vital sign Monitor':
              VitalSignMonitor++
              break
          }
        }
      })
      const testDeviceList = []
      /*testDeviceList.push(
        { x: 'AnesMachine', y: AnesMachine },
        { x: 'Defibrillator', y: Defibrillator },
        { x: 'FiberOptic', y: FiberOptic },
        { x: 'PulseOximeter', y: PulseOximeter },
        { x: 'TourniquetSystem', y: TourniquetSystem },
        { x: 'Ultrasound', y: Ultrasound },
        { x: 'VideoLaryngoScope', y: VideoLaryngoScope },
        { x: 'VitalSignMonitor', y: VitalSignMonitor },
      )*/
      deviceList.push(
        { x: AnesMachine, y: AnesMachine },
        { x: Defibrillator, y: Defibrillator },
        { x: FiberOptic, y: FiberOptic },
        { x: PulseOximeter, y: PulseOximeter },
        { x: TourniquetSystem, y: TourniquetSystem },
        { x: Ultrasound, y: Ultrasound },
        { x: VideoLaryngoScope, y: VideoLaryngoScope },
        { x: VitalSignMonitor, y: VitalSignMonitor },
      )
      setDeviceDashBoard(deviceList)

      //testdashboardData.push(...Year)
      //testdashboardData.push(...testDeviceList)
      //console.log(testDeviceList)
      console.log("year" ,Year)
      setDashBoardData(Year)
    })
    //console.log(DashBoardData)

    console.log('_________________________', testdashboardData)
    //return testdashboardData
  }
  const FetchMonthDashBoard = () => {
    resetDashBoard()
    userRef.once('value', (snapshot) => {
      console.log(dayjs(selectedDate).format('YYYY/MM'))
      const userSnap = snapshot.val()
      const hist = []
      let totalDay = dayjs(selectedDate).endOf('month').format('DD')
      console.log('number of day in this month >> ', totalDay)
      let count = 0
      for (let key in userSnap) {
        if (userSnap[key].userRole == 'borrower') {
          const HistoryRef = userRef
            .child(key)
            .child('History/BorrowingHistory')
            .child(dayjs(selectedDate).format('YYYY/MM'))
            .orderByKey()
            .once('value', (snapshot) => {
              const month = snapshot.val()
              for (let day in month) {
                for (let person in month[day]) {
                  for (let device in month[day][person]) {
                    count++
                    let deviceType = split(
                      month[day][person][device]['UID'],
                      '/',
                    )[0]
                    hist.push({ [day]: { device, deviceType } })
                  }
                }
              }
            })
        }
      }
      // console.log('hist', hist)
      hist.map((item) => {
        console.log(item)
        for (let key in item) {
          switch (key) {
            case '01':
              first++
              break
            case '02':
              second++
              break
            case '03':
              third++
              break
            case '04':
              fourth++
              break
            case '05':
              fifth++
              break
            case '06':
              sixth++
              break
            case '07':
              seventh++
              break
            case '08':
              eighth++
              break
            case '09':
              ninth++
              break
            case '10':
              tenth++
              break
            case '11':
              eleventh++
              break
            case '12':
              twelveth++
              break
            case '13':
              thirtheenth++
              break
            case '14':
              fourteenth++
              break
            case '15':
              fifteenth++
              break
            case '16':
              sixteenth++
              break
            case '17':
              second++
              break
            case '18':
              eighteenth++
              break
            case '19':
              nineteenth++
              break
            case '20':
              twentieth++
              break
            case '21':
              twentyFirst++
              break
            case '22':
              twentySecond++
              break
            case '23':
              twentyThird++
              break
            case '24':
              twentyForth++
              break
            case '25':
              twentyFifth++
              break
            case '26':
              twentySixth++
              break
            case '27':
              twentySeventh++
              break
            case '28':
              twentyEighth++
              break
            case '29':
              twentyNinth++
              break
            case '30':
              thirtieth++
              break
            case '31':
              thirtieFirst++
              break
          }
        }
      })
      const month = []
      month.push(
        { x: 1, y: first },
        { x: 2, y: second },
        { x: 3, y: third },
        { x: 4, y: fourth },
        { x: 5, y: fifth },
        { x: 6, y: sixth },
        { x: 7, y: seventh },
        { x: 8, y: eighth },
        { x: 9, y: ninth },
        { x: 10, y: tenth },
        { x: 11, y: eleventh },
        { x: 12, y: twelveth },
        { x: 13, y: thirtheenth },
        { x: 14, y: fourteenth },
        { x: 15, y: fifteenth },
        { x: 16, y: sixteenth },
        { x: 17, y: seventeenth },
        { x: 18, y: eighteenth },
        { x: 19, y: nineteenth },
        { x: 20, y: twentieth },
        { x: 21, y: twentyFirst },
        { x: 22, y: twentySecond },
        { x: 23, y: twentyThird },
        { x: 24, y: twentyForth },
        { x: 25, y: twentyFifth },
        { x: 26, y: twentySixth },
        { x: 27, y: twentySeventh },
        { x: 28, y: twentyEighth },
      )
      if (totalDay == 29) {
        month.push({ x: 29, y: twentyNinth })
      }
      if (totalDay == 30) {
        month.push({ x: 29, y: twentyNinth }, { x: 30, y: thirtieth })
      }
      if (totalDay == 31) {
        month.push(
          { x: 29, y: twentyNinth },
          { x: 30, y: thirtieth },
          { x: 31, y: thirtieFirst },
        )
      }

      //map device data
      hist.map((item) => {
        for (let key in item) {
          switch (item[key].deviceType) {
            case 'Anes Machine':
              AnesMachine++
              break
            case 'Defibrillator':
              Defibrillator++
              break
            case 'Fiber Optic':
              FiberOptic++
              break
            case 'Pulse oximeter':
              PulseOximeter++
              break
            case 'Tourniquet System':
              TourniquetSystem++
              break
            case 'Ultrasound':
              Ultrasound++
              break
            case 'Video LaryngoScope':
              VideoLaryngoScope++
              break
            case 'Vital sign Monitor':
              VitalSignMonitor++
              break
          }
        }
      })
      const deviceList = []
      deviceList.push(
        { x: AnesMachine, y: AnesMachine },
        { x: Defibrillator, y: Defibrillator },
        { x: FiberOptic, y: FiberOptic },
        { x: PulseOximeter, y: PulseOximeter },
        { x: TourniquetSystem, y: TourniquetSystem },
        { x: Ultrasound, y: Ultrasound },
        { x: VideoLaryngoScope, y: VideoLaryngoScope },
        { x: VitalSignMonitor, y: VitalSignMonitor },
      )

      setDeviceDashBoard(deviceList)
      setDashBoardData(month)
    })
  }
  const FetchWeekDashBoard = () => {
    resetDashBoard()
    userRef.once('value', (snapshot) => {
      console.log(dayjs(selectedDate).format('YYYY/MM'))
      const userSnap = snapshot.val()
      const hist = []
      console.log(
        'DATE LOG >>> ',
        dayjs(selectedDate).startOf('week').format('YYYY/MM/DD'),
        ' TO ',
        dayjs(selectedDate).endOf('week').format('YYYY/MM/DD'),
      )
      Sun = dayjs(selectedDate).startOf('week').format('DD')
      Mon = dayjs(selectedDate).startOf('week').add(1, 'day').format('DD')
      Tues = dayjs(selectedDate).startOf('week').add(2, 'day').format('DD')
      Wed = dayjs(selectedDate).startOf('week').add(3, 'day').format('DD')
      Thu = dayjs(selectedDate).startOf('week').add(4, 'day').format('DD')
      Fri = dayjs(selectedDate).startOf('week').add(5, 'day').format('DD')
      Sat = dayjs(selectedDate).endOf('week').format('DD')
      let count = 0
      for (let key in userSnap) {
        if (userSnap[key].userRole == 'borrower') {
          const HistoryRef = userRef
            .child(key)
            .child('History/BorrowingHistory')
            .child(dayjs(selectedDate).format('YYYY/MM'))
            .orderByKey()
            .startAt(dayjs(selectedDate).startOf('week').format('DD'))
            .endAt(dayjs(selectedDate).endOf('week').format('DD'))
            .once('value', (snapshot) => {
              const week = snapshot.val()

              for (let day in week) {
                for (let person in week[day]) {
                  for (let device in week[day][person]) {
                    count++
                    let deviceType = split(
                      week[day][person][device]['UID'],
                      '/',
                    )[0]
                    hist.push({ [day]: { device, deviceType } })
                  }
                }
              }
            })
        }
      }
      const week = []
      hist.map((item) => {
        for (let key in item) {
          switch (key) {
            case Sun:
              SunVal++
              break
            case Mon:
              MonVal++
              break
            case Tues:
              TuesVal++
              break
            case Wed:
              WedVal++
              break
            case Thu:
              ThuVal++
              break
            case Fri:
              FriVal++
              break
            case Sat:
              SatVal++
              break
          }
        }
      })
      //test
      week.push(
        { x: Sun, y: SunVal },
        { x: Mon, y: MonVal },
        { x: Tues, y: TuesVal },
        { x: Wed, y: WedVal },
        { x: Thu, y: ThuVal },
        { x: Fri, y: FriVal },
        { x: Sat, y: SatVal },
      )
      const deviceList = []
      hist.map((item) => {
        for (let key in item) {
          //console.log("LOOOOOOP",item[key].deviceType)
          switch (item[key].deviceType) {
            case 'Anes Machine':
              AnesMachine++
              break
            case 'Defibrillator':
              Defibrillator++
              break
            case 'Fiber Optic':
              FiberOptic++
              break
            case 'Pulse oximeter':
              PulseOximeter++
              break
            case 'Tourniquet System':
              TourniquetSystem++
              break
            case 'Ultrasound':
              Ultrasound++
              break
            case 'Video LaryngoScope':
              VideoLaryngoScope++
              break
            case 'Vital sign Monitor':
              VitalSignMonitor++
              break
          }
        }
      })
      deviceList.push(
        { x: AnesMachine, y: AnesMachine },
        { x: Defibrillator, y: Defibrillator },
        { x: FiberOptic, y: FiberOptic },
        { x: PulseOximeter, y: PulseOximeter },
        { x: TourniquetSystem, y: TourniquetSystem },
        { x: Ultrasound, y: Ultrasound },
        { x: VideoLaryngoScope, y: VideoLaryngoScope },
        { x: VitalSignMonitor, y: VitalSignMonitor },
      )
      setDashBoardData(week)
      setDeviceDashBoard(deviceList)
    })
  }

  const FetchDashBoard = (selectedDashBoardMode) => {
    if (selectedDashBoardMode == 'YEAR') {
      FetchYearDashBoard()
      setMarkerSpacing(0)
    }
    if (selectedDashBoardMode == 'MONTH') {
      FetchMonthDashBoard()
      setMarkerSpacing(2)
    }
    if (selectedDashBoardMode == 'WEEK') {
      FetchWeekDashBoard()
      setMarkerSpacing(0)
    }
    DashBoardData.map((item) => console.log(item))
    renderX("YEAR").map((item) => console.log(item))
  }

  const onChange = (event, selectingDate) => {
    const currentDate = selectingDate || date
    setShow(Platform.OS === 'ios')
    setSelectedDate(currentDate)
    setShow(false)
  }
  const showMode = (currentMode) => {
    setShow(true)
    setMode(currentMode)
  }
  const showDatepicker = () => {
    showMode('date')
  }

  const computeInterval = () => {
      let input = 0
      for (let key in DashBoardData) {
        input += DashBoardData[key].y
      }
      input = parseInt(input / 12)
      if (input >= 1000) {
        return 200
      }
      if (input > 100) {
        return 100
      }
      if (input >= 50) {
        return 50
      }
      if (input >= 30) {
        return 30
      }
      if (input >= 15) {
        return 20
      }
      if (input >= 10) {
        return 10
      }
      if (input < 10) {
        return 2
      }
    
  
    if (selectedDashBoardMode == 'MONTH') {
      let input = 0
      for (let key in DashBoardData) {
        input += DashBoardData[key].y
      }
      if (DashBoardData.length == 28) {
        input = parseInt(input / 28)
      }
      if (DashBoardData.length == 29) {
        input = parseInt(input / 29)
      }
      if (DashBoardData.length == 30) {
        input = parseInt(input / 30)
      }
      if (DashBoardData.length == 31) {
        input = parseInt(input / 31)
      }
      if (input >= 1000) {
        return 200
      }
      if (input > 100) {
        return 100
      }
      if (input >= 50) {
        return 50
      }
      if (input >= 30) {
        return 30
      }
      if (input >= 15) {
        return 20
      }
      if (input >= 10) {
        return 10
      }
      if (input < 10) {
        return 2
      }
    }

    if (selectedDashBoardMode == 'WEEK') {
      const input = parseInt(
        (SunVal + MonVal + TuesVal + WedVal + TuesVal + FriVal + SatVal) / 7,
      )
      if (input >= 1000) {
        return 200
      }
      if (input > 100) {
        return 100
      }
      if (input >= 50) {
        return 50
      }
      if (input >= 30) {
        return 30
      }
      if (input >= 15) {
        return 20
      }
      if (input >= 10) {
        return 10
      }
      if (input < 10) {
        return 2
      }
    }
  }
  
  useEffect(() => {
    //FetchYearDashBoard()
    //_TEST_FetchYearDashBoard('2020/12/16', FetchYearDashBoard())
    console.log('useEffect')
    FetchDashBoard(selectedDashBoardMode)
    //INIT_DASHBOARD(YEAR)
    //console.log("SELECTED >> ",selectHistoryList)
    //console.log(DashBoardData)
  }, [selectedDate, selectedDashBoardMode])

  return (
    <View style={{ justifyContent: 'center' }}>
      <View
        style={{
          width: '60%',
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          margin: 10,
          alignSelf: 'center',
        }}>
        <Button
          // color="#2C9086:"
          color="white"
          title="WEEK"
          titleStyle={{ color: 'black' }}
          buttonStyle={{ backgroundColor: 'white' }}
          containerStyle={{ elevation: 8 }}
          onPress={() => {
            console.log('WEEK PRESS'), setSelectedDashBoardMode('WEEK')
          }}
        />
        <Button
          title="MONTH"
          color="white"
          titleStyle={{ color: 'black' }}
          buttonStyle={{ backgroundColor: 'white' }}
          containerStyle={{ elevation: 8 }}
          onPress={() => {
            console.log('MONTH PRESS'), setSelectedDashBoardMode('MONTH')
          }}
        />
        <Button
          color="white"
          title="YEAR"
          titleStyle={{ color: 'black' }}
          buttonStyle={{ backgroundColor: 'white' }}
          containerStyle={{ elevation: 8 }}
          onPress={() => {
            console.log('YEAR PRESS'), setSelectedDashBoardMode('YEAR')
          }}
        />
      </View>
      <TouchableOpacity
        onPress={showDatepicker}
        style={{ alignSelf: 'center' }}>
        {selectedDashBoardMode == 'YEAR' ? (
          <Text> Borrowed device in {dayjs(selectedDate).format('YYYY')}</Text>
        ) : null}
        {selectedDashBoardMode == 'MONTH' ? (
          <Text>
            Borrowed device in {dayjs(selectedDate).format('MMMM YYYY')}{' '}
          </Text>
        ) : null}
        {selectedDashBoardMode == 'WEEK' ? (
          <Text>
            Borrowed device in{' '}
            {dayjs(selectedDate).startOf('week').format('DD MMM YYYY')} -
            {dayjs(selectedDate).endOf('week').format('DD MMM YYYY')}
          </Text>
        ) : null}
      </TouchableOpacity>
      {/* Upper part including with pie chart */}
      <View>
        {DeviceDashBoard.length ? (
          <View>
            <VictoryPie
              colorScale={colorPallete}
              width={250}
              height={250}
              style={{
                labels: {
                  fill: 'black',
                  fontSize: 15,
                  padding: 15,
                },
              }}
              data={DeviceDashBoard}
            />
          </View>
        ) : null}
        {/*? <View style={{top:-130 , paddingLeft:20}}>
          <Text>There is no data here</Text></View> : null*/}
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={selectedDate}
            timeZoneOffsetInMinutes={0}
            mode={mode}
            is24Hour={true}
            display="default"
            onChange={onChange}
          />
        )}
        {/* legend section on the right */}
        <Text
          style={{
            position: 'absolute',
            top: -15,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#f67676',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 20,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          Anes Machine
          {/* Anes Machine : {DeviceDashBoard.length ? DeviceDashBoard[0].x : '0'}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[0].x == null
              ? '0'
              : null} */}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 7,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#00C288',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 43,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          Defibrillator
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 33,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#F6A962',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 70,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          Fiber Optic
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 59,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#62AFF6',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 95,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          Pulse Oximeter
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 85,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#B562F6',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 120,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          Tourniquet System
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 108,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#F6D862',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 145,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          Ultrasound{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 135,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#BDB5B1',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 170,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          {' '}
          Video Laryngoscope
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 110 + 52,
            left: '46%',
            paddingTop: 0,
            paddingLeft: 30,
            fontSize: 62,
            color: '#34251D',
          }}>
          {' '}
          ∙{' '}
        </Text>
        <Text
          style={{
            position: 'absolute',
            top: 144 + 52,
            left: '50%',
            paddingTop: 0,
            paddingLeft: 48,
            color: 'black',
          }}>
          {' '}
          Vital Sign Monitor
        </Text>

        {/* <Text
            style={{
              position: 'absolute',
              top: 60,
              left: '50%',
              paddingLeft: 30,
              color: 'blue',
            }}>
            Defibrillator :{' '}
            {DeviceDashBoard.length ? DeviceDashBoard[1].x : '0'}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[1].x == null
              ? '0'
              : null}
          </Text>
          <Text
            style={{
              position: 'absolute',
              left: '50%',
              top: 80,
              paddingLeft: 30,
              color: 'green',
            }}>
            Fiber Optic : {DeviceDashBoard.length ? DeviceDashBoard[2].x : 'O'}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[2].x == null
              ? '0'
              : null}
          </Text>
          <Text
            style={{
              position: 'absolute',
              top: 100,
              left: '50%',
              paddingLeft: 30,
              color: 'olive',
            }}>
            Defibrillator : {DeviceDashBoard.length ? DeviceDashBoard[3].x : ''}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[3].x == null
              ? '0'
              : null}
          </Text>
          <Text
            style={{
              position: 'absolute',
              top: 120,
              left: '50%',
              paddingLeft: 30,
              color: 'brown',
            }}>
            Tourniquet System :{' '}
            {DeviceDashBoard.length ? DeviceDashBoard[4].x : ''}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[4].x == null
              ? '0'
              : null}
          </Text>
          <Text
            style={{
              position: 'absolute',
              top: 140,
              left: '50%',
              paddingLeft: 30,
              color: 'pink',
            }}>
            {' '}
            Ultrasound : {DeviceDashBoard.length
              ? DeviceDashBoard[5].x
              : ' '}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[5].x == null
              ? '0'
              : null}
          </Text>
          <Text
            style={{
              position: 'absolute',
              top: 160,
              left: '50%',
              paddingLeft: 30,
              color: 'gray',
            }}>
            {' '}
            Video LaryngoScope :{' '}
            {DeviceDashBoard.length ? DeviceDashBoard[6].x : ' '}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[6].x == null
              ? '0'
              : null}
          </Text>
          <Text
            style={{
              position: 'absolute',
              top: 180,
              left: '50%',
              paddingLeft: 30,
              color: 'lime',
            }}>
            {' '}
            VitalSignMonitor :{' '}
            {DeviceDashBoard.length ? DeviceDashBoard[7].x : ' '}{' '}
            {DeviceDashBoard.length == 0 || DeviceDashBoard[7].x == null
              ? '0'
              : null}
          </Text> */}
      </View>
      <View style={{ paddingLeft: 30 }}>
        {/*
      <Text>MODE : {selectedDashBoardMode} </Text>
      <Text>All BorrowingItem : </Text>
      <Text>Data From {dayjs(FirstDate).format('DD/MM/YYYY')} to {dayjs(LastDate).format('DD/MM/YYYY')}</Text>
      <Text>{dayjs().format('DD/MM/YYYY')}</Text>*/}
      </View>
      {DashBoardData.length ? (
        <View style={{ flex: 1 }}>
          <SlideBarChart
            scrollable
            barSelectedColor={'#2C9086'}
            key={'slidebar'}
            data={DashBoardData}
            // scrollabl
            axisWidth={32}
            axisHeight={16}
            chartPaddingTop={16}
            alwaysShowIndicator={true}
            height={220}
            fillColor="lightgray"
            yAxisProps={{
              numberOfTicks: 4,
              axisLabel: 'Devices',
              axisLabelAlignment: 'top',
              fullBaseLine: true,
              //showBaseLine: true,
              interval: computeInterval(),
              axisLabelStyle: { fontSize: 10 },
            }}
            xAxisProps={{
              axisMarkerLabels: /*DashBoardData.map((item) => item.x+""),*/ renderX(selectedDashBoardMode).map((item) => item.x),
              adjustForSpecialMarkers: true,
              markerSpacing: markerSpacing,
              // markerTopPadding: 8,
            }}
            toolTipProps={{
              borderRadius: 100,
              lockTriangleCenter: true,
              toolTipTextRenderers: [
                
                () => ({ text: 'Device(s)' }),
                ({ selectedBarNumber }) => ({
                  text:
                    DashBoardData[selectedBarNumber].y != null
                      ? DashBoardData[selectedBarNumber].y + ''
                      : '0',
                }),
              ],
            }}></SlideBarChart>
        </View>
      ) : null}
    </View>
  )
}

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  approved: {
    color: '#28CD1A',
  },
  rejected: {
    color: '#ff0000',
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  sectionHeader: {
    color: 'grey',
    textTransform: 'uppercase',
    fontSize: 15,
    marginLeft: 5,
    marginTop: 16,
    marginRight: 16,
  },
})
export default DashBoard
