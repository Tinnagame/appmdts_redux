import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, ScrollView, Button, Alert } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { userRef, locationRef, deviceRef } from '~/configs/firestore'
import { Card, Input } from 'react-native-elements'
import { Picker } from '@react-native-community/picker'
import moment from 'moment'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import auth from '@react-native-firebase/auth'
import { INIT_USER } from '../../../../Functions'
import lodash, { set,map } from 'lodash'
import dayjs from 'dayjs'
const SecondCheckoutBorrowingForm = (props) => {
  const navigation = useNavigation()
  const { location, patientName, hospitalNumber, device } = props.route.params
  const [borrowerName, setborrowerName] = useState('0')
  const [borrowerTel, setborrowerTel] = useState('0')
  const [locations, setLocationList] = useState([])
  const [selectedLocation, setSelectedLocation] = useState('0')
  const [serialNumber, setSerialNumber] = useState('')
  const [borrowingList, setBorrowingList] = useState([])
  const [borrowingPathList, setBorrowingPathList] = useState([])
  const [devicePathList, setDevicePathList] = useState([])

  const ChangeCurrentStatusInDatabase = () => {
    for (let key in borrowingPathList) {
      console.log('key loop in borrowingPathList >> ', borrowingPathList[key])
      deviceRef.child('deviceType/' + borrowingPathList[key]).update({
        currentStatus: 'In-use',
        currentLocation: location,
        borrowerUID: auth().currentUser.uid,
        borrowerName: borrowerName,
        borrowerTel: borrowerTel,
        patientName: patientName,
        patientHospitalNumber: hospitalNumber,
        borrowingDate: dayjs().format("YYYY/MM/DD, LT ")
      })

      deviceRef
        .child('deviceType/' + borrowingPathList[key])
        .once('value', (snapshot) => {
          const data = snapshot.val()
          console.log(data)
          userRef
            .child(auth().currentUser.uid)
            .child('Items/In-use/' + borrowingPathList[key])
            .update({
              currentLocation: data.currentLocation,
              currentStatus: data.currentStatus,
              deviceBrand: data.deviceBrand,
              deviceModel: data.deviceModel,
              deviceSerialNumber: data.deviceSerialNumber,
              patientName:data.patientName ,
              borrowingDate: dayjs().format("YYYY/MM/DD, LT ")
            })

          /*
    userRef.child(uid).child("Items/In-use").push({
      
    })*/
        })
    }
  }
/*
  const AddingBorrowingHistoryToFirebase = (uid, borrowingPathList) => {
    const today = moment().format('YYYY/MM/DD')
    const historyRef = userRef
      .child(auth().currentUser.uid)
      .child('History/BorrowingHistory')
      .child(today)
      .child(auth().currentUser.uid)
    for (let key in borrowingPathList) {
      console.log('', borrowingPathList[key])

      deviceRef
        .child('deviceType/' + borrowingPathList[key])
        .once('value', (snapshot) => {
          const data = snapshot.val()

          historyRef.child(borrowingPathList[key]).update({
            currentLocation: data.currentLocation,
            currentStatus: data.currentStatus,
            deviceBrand: data.deviceBrand,
            deviceModel: data.deviceModel,
            deviceSerialNumber: data.deviceSerialNumber,
          })
        })
    }
  }
*/
  const validateDevice = (device, DevicePathList) => {
    const newPathList = DevicePathList
    if (DevicePathList.length == 0 && device) {
      DevicePathList.push(device)
      return true

    }
    
    let deviceAlreadyExist = false
    /*
    newPathList.forEach(path => {
      deviceAlreadyExist = device === path || deviceAlreadyExist 
    })*/
    for(let i=0;i< DevicePathList.length; i++) {
      if(deviceAlreadyExist) break;
      deviceAlreadyExist = device === DevicePathList[i]
    }

    if (!deviceAlreadyExist) {
      newPathList.push(device)
      setDevicePathList(newPathList)
      return true
    }
    Alert.alert('', 'This device is already added')

    return false
    
  }
  
  const FetchDeviceData = () => {
    if (device && validateDevice(device, devicePathList)) {
      deviceRef.child('deviceType/' + device).once('value', (snapshot) => {
        
        const ref = snapshot.val()
        const deviceType = device.split('/')[0]
        /*
        if(ref){
        borrowingList.push({ deviceType, ...ref })
        borrowingPathList.push(device)

        
        }*/

        
        if(ref){
        let tmpBL = [...borrowingList]
          tmpBL.push({ deviceType, ...ref })
          let tmpBPL = [...borrowingPathList]
          tmpBPL.push(device)

          setBorrowingList(tmpBL)
          setBorrowingPathList(tmpBPL)
        }
      },
      )
    } else {
      console.log('cannot fetch device')
    }
    
  }



  useEffect(() => {
    console.log('DEVICE >> ', device)
    INIT_USER(auth().currentUser.uid, setborrowerName, setborrowerTel)
    FetchDeviceData()
  
  
  }, [device])

  return (

    <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      {
      <Card>
        <View style={{ marginTop: 0 }}>
          <Text style={styles.headerText}>Borrower name</Text>
          <Text style={styles.contentText}>{borrowerName}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Borrower phone number</Text>
          <Text style={styles.contentText}>{borrowerTel}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Location</Text>
          <Text style={styles.contentText}>{location}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Patient name</Text>
          <Text style={styles.contentText}>{patientName}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Hospital number</Text>
          <Text style={styles.contentText}>{hospitalNumber}</Text>
        </View>
      </Card>
      }

      <View style={{ marginTop: 10 }}>
        <TouchableOpacity
          onPress={() => {
              navigation.navigate('CheckoutBorrowingQRcodeScanner')
          }}>
          <Card>
            <View style={{ flexDirection: 'row' }}>
              <Text
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  fontSize: 20,
                  paddingLeft: 100,
                }}>
                ADD DEVICE
              </Text>
              <Icon name="qrcode-scan" size={30}></Icon>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
      
        <ScrollView style={{ marginTop: 16, flex: 1 }}>
          <Card>
            <Text>{device || 'null'}</Text>
          </Card>

          {!!borrowingList.length
            && borrowingList.map((Device, index) => (
                  <TouchableOpacity>
                    <Card key={Device.deviceSerialNumber}>
                      <View>
                        <Text style={{ paddingBottom: 2 }}>
                          {Device.deviceType || '-'}
                        </Text>
                        <Text style={{ color: 'gray' }}>Device Model</Text>
                        <Text>{Device.deviceModel || '-'}</Text>
                        <Text style={{ color: 'gray' }}>S/N</Text>
                        <Text> {Device.deviceSerialNumber || '-'}</Text>
                      </View>
                    </Card>
                  </TouchableOpacity>
                ))}
        </ScrollView>
      
      {
      <View>
        <Button
          title="CONFIRM"
          color="#2C9086"
          onPress={() => {
            Alert.alert(
              'Confirm',
              'ADD',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'OK',
                  onPress: () => {
                    
                      ChangeCurrentStatusInDatabase(),
                    Alert.alert('', 'COMPLETE')
                    navigation.navigate('Home')
                  },
                },
              ],
            )
          }}
        />
      </View>
        }
    </View>
  )
}

export default SecondCheckoutBorrowingForm

const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
})
