import React , {useEffect,useState}from 'react'
import { View, Text,StyleSheet,ScrollView,Button, Alert } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { userRef ,locationRef, deviceRef, db} from '~/configs/firestore'
import { Card,Input,  } from 'react-native-elements'
import { Picker } from '@react-native-community/picker'
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import auth from '@react-native-firebase/auth'
import * as Animatable from 'react-native-animatable'
import {INIT_USER,INIT_LOCATION} from '../../../../Functions'
const FirstCheckoutBorrowingForm = (props) => {
 const navigation = useNavigation()
 //const {uid,device}= props.route.params
 
 const [borrowerName, setborrowerName] = useState('0')
 const [borrowerTel, setborrowerTel] = useState('0')
 const [locations,setLocationList] = useState([]);
 const [selectedLocation, setSelectedLocation] = useState('0')
 const [serialNumber,setSerialNumber] = useState('')
 const [patientName,setPatientName] = useState('')
 const [hospitalNumber,setHospitalNumber] = useState('')
 const [borrowingList,setBorrowingList] = useState([])
 const [text,setText] = useState('0')
 const [borrowingPathList,setBorrowingPathList] = useState([])

 const [isEmpty, setIsEmpty] = useState(false)
 const [inputLocation, setInputLocation] = useState(false)
 const thisUser = auth().currentUser
 const checkHospitalNumber = hospitalNumber != ''
 const checkPatientName = patientName != ''
 const checkLocation = selectedLocation != '0'

 const devicesList = []

/*
  const onScan = (data) =>{
    setText(data)
    /*
    deviceRef.child('deviceType/',device).once('value',(snapshot) => {
      const ref = snapshot.val()
      console.log(ref)
    })*/
    /*
    userRef.child(data).once('value',(snapshot) => {
      const ref = snapshot.val()
      console.log(ref)
    })*/
    
//  }
/*
  const Borrow = () => {
    ChangeCurrentStatusInDatabase
    Alert.alert()
  }*/
 /*
  const ChangeCurrentStatusInDatabase = () => {
  for(let key in borrowingPathList) {
    console.log("key lopp in borrowingPathList >> ",borrowingPathList[key])
    deviceRef.child("deviceType/"+borrowingPathList[key]).update({
      currentStatus: "In-use",
      currentLocation: selectedLocation,
      borrowerUID: uid,
      borrowerName: borrowerName,
      borrowerTel: borrowerTel
    })
    /*
    deviceRef.child("deviceType/"+borrowingPathList[key]).once('value',(snapshot)=>{
      const data = snapshot.val();
      console.log(data)
      userRef.child(uid).child("Items/In-use/"+borrowingPathList[key]).update({
        currentLocation: data.currentLocation,
        currentStatus: data.currentStatus,
        deviceBrand: data.deviceBrand,
        deviceModel: data.deviceModel,
        deviceSerialNumber: data.deviceSerialNumber
    })
*/

    /*
    userRef.child(uid).child("Items/In-use").push({
      
    })*/

  /*  
  })}
  }
  /*
  const AddingBorrowingHistoryToFirebase = () => {
    const today = moment().format('YYYY/MM/DD')
    const historyRef = userRef.child(uid).child("History/BorrowingHistory").child(today).child(uid);
    for(let key in borrowingPathList) {
      console.log("",borrowingPathList[key])
      
    deviceRef.child("deviceType/"+borrowingPathList[key]).once('value',(snapshot)=>{
      const data = snapshot.val();
    
      
    
    
      historyRef.child(borrowingPathList[key]).update({
        currentLocation: data.currentLocation,
          currentStatus: data.currentStatus,
          deviceBrand: data.deviceBrand,
          deviceModel: data.deviceModel,
          deviceSerialNumber: data.deviceSerialNumber
      })
    
    })
    
  }
  }*/
  const _TEST_INIT_USER = (input,func) => {
    var myJSON = JSON.stringify(func)
    console.log('**********TEST*********')
    console.log('CASE 01 - INIT_USER    ')
    console.log('INPUT :'+ input)
    console.log('EXPECT [{"userName":"Jiramed Withunsapphasiri","userTel":"092-686-5964"}]')
    console.log("RESULT "+myJSON)
    console.log('**********TEST*********')
  }
  useEffect(() => {
    

    //console.log("BLI",borrowingList)
    //console.log("UID",thisUser);
    //SET Borrower
    console.log(auth().currentUser.uid)
    //_TEST_INIT_USER("TNchT1pgfDbNBhAEmXrXuMPzZfL2",INIT_USER("TNchT1pgfDbNBhAEmXrXuMPzZfL2",setborrowerName,setborrowerTel))
    INIT_USER(auth().currentUser.uid,setborrowerName,setborrowerTel);
    INIT_LOCATION('PatientWard',setLocationList);
    /*
    deviceRef.child("deviceType/"+device).once('value',(snapshot) => {
      const ref = snapshot.val()
      console.log("scanned",ref)
      //borrowingList.push({"deviceType/"+device,...ref})
      if(ref != null){
        borrowingList.push({"ref":ref})
        borrowingPathList.push(device)
        
        setBorrowingList(borrowingList)
        setBorrowingPathList(borrowingPathList)
        for(let key in borrowingList){
          console.log("key >> ",borrowingList[key])
        }
        console.log(borrowingList)
      }
      
    })*/

    

  },[])
  return (
    
    <View style={{ flex: 1, backgroundColor: '#f3f3f3' }}>
      <Card>
       
        <View style={{marginTop:0}}>
          <Text style={styles.headerText}>Borrower name</Text>
          <Text style={styles.contentText}>{borrowerName}</Text>
        </View>
        <View style={styles.textGap}>
          <Text style={styles.headerText}>Borrower phone number</Text>
          <Text style={styles.contentText}>{borrowerTel}</Text>
        </View>

     </Card>
     <Card>
          <View style={{ marginTop: 1 }}>
              <Text style={{color:'gray'}}> Location </Text>
              <Picker
                style={{ height: 50, width: '100%' }}
                mode="dropdown"
                selectedValue={selectedLocation}
                onValueChange={(itemValue) => {setSelectedLocation(itemValue)}}>


                <Picker.Item label="select location" value="0" />
                {locations ? locations.map((location, index) => 
            
                <Picker.Item label={location.name} value={location.name} key={index}/> ) : '' }
              
              </Picker>
            </View></Card>

     <View style={{marginTop:30,borderWidth:2 ,borderColor:'gray' ,width:"90%",alignSelf:'center'}}>
      <Input style={{padding:10}}
      placeholder="Patient name"
      value={patientName}
      onChangeText={setPatientName}
      ></Input>
      <Input style={{padding:10}}
      placeholder="HN (Hospital Number)"
      value={hospitalNumber}
      onChangeText={setHospitalNumber}
      ></Input>
    </View>
        
    
    <View
            style={{
              color:"red",
              flex: 1,
              alignItems: 'center',
              // borderWidth: 1,
              height: 20,
            }}>
            {isEmpty ? (
              <Animatable.View animation="fadeInLeft" duration={1000}>
                <Text style={styles.alertText}>
                  please input patient name and hospital number
                </Text>
              </Animatable.View>
            ) : null}
            {inputLocation ? (
              <Animatable.View animation="fadeInLeft" duration={1000}>
                <Text style={styles.alertText}>
                  Please select new location 
                </Text>
              </Animatable.View>
            ) : null}
          </View>
          <View style={{paddingTop:30, alignSelf:'center',width: '80%'}}>
      <Button styles={{marginTop:10}}title="CONFIRM" color="#2C9086" 
      
      onPress={()=> {
        if(!patientName||!hospitalNumber){
          setIsEmpty(true)
          
        } else if(!checkLocation) {
          setInputLocation(true)
        } else {
          navigation.navigate('SecondCheckoutBorrowingForm',{
            location:selectedLocation,
            patientName:patientName,
            hospitalNumber:"HN"+hospitalNumber,
            device:null
          })
        }
        
      }}
   />
    </View>

      </View>
      
  )
}

export default FirstCheckoutBorrowingForm


const styles = StyleSheet.create({
  textGap: {
    marginTop: 24,
  },
  headerText: {
    color: 'gray',
  },
  contentText: {
    marginLeft: 8,
    marginTop: 8,
  },
  alertText: {
    color: 'red',
    alignSelf: 'center',
    paddingTop: 4,
    paddingBottom: 4,
    paddingLeft: 4,
    paddingRight: 4,
    borderColor: 'red',
  },
})
