import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  Button,
  StyleSheet,
  TouchableOpacity,
  ScrollView,Alert,Modal
} from 'react-native'

import { Icon, Card } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native'
import { Picker } from '@react-native-community/picker'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import auth from '@react-native-firebase/auth'
//firebase 
import {db,deviceRef, locationRef,borrowerRef, userRef} from '../../../configs/firestore'
import {LocalNotification, pushNoti} from '../../../configs/pushNotification'
import dayjs from 'dayjs'
const BorrowerDeviceRequest = () => {
  const navigation = useNavigation()
  //firebase 
const [locations,setLocationList] = useState([]);
const [temp,setTempList] = useState([]);
const user = auth().currentUser;
const [name,setname] = useState('')
const [tel,setTel] = useState('')
const [DeviceTypeList,setDeviceTypeList] = useState([])
const [countAmount,setCountAmount] = useState([])
const [selectedType, setSelectedType] = useState('0')
const [selectedAmount, setSelectedAmount] = useState('0')
const [selectedLocation, setSelectedLocation] = useState('0')
const [DeviceList,setDeviceList] = useState([])
/*
  RTB = Real Time Database

*/


const fetchTime =() => {
  
}
useEffect(() => {
  //saveNotificationToDatabase()
  //Retrive user from firebase 
  const UserRef = userRef.child(user.uid)
    UserRef.once('value',(snapshot) => {
      const ref = snapshot.val();
     // console.log(ref)
      
      setname(ref.userName)
      setTel(ref.userTel)
      
    })

  //Retrive device from firebase 
  deviceRef.child("deviceType").on('value', (snapshot) => {
    const devices = snapshot.val();
   // console.log("device",devices)
    const deviceList = [];
    const TypeList = [];
    for (let type in devices) {
      //console.log("key",type)
      TypeList.push({type,type})
      //console.log(TypeList)
      deviceList.push({type:devices[type] });
    }
   // console.log("deviceList >>",deviceList)
    setDeviceList(deviceList);
    setDeviceTypeList(TypeList)
  
  });
  //Retrive location from RTB 
  locationRef.on('value', (snapshot) => {
    const locations = snapshot.val();
    //console.log("LOCAtiON >>>",locations)
    const locationList = [];
    for (let name in locations) {
      //console.log(locations[name])
      locationList.push({"name":locations[name]});

    }
    setLocationList(locationList);
    //console.log(locationList);
  },);
  const BorrowerRef = userRef.child(user.uid)
  const Borrowed_itemsRef = BorrowerRef.child('Items')
  const tempBorrowRef = Borrowed_itemsRef.child('requesting')

  //tempBorrowRef = List of temp requesting device
  tempBorrowRef.on('value',(snapshot) => {
      const temp = snapshot.val();
      const tempList = [];
      for (let id in temp) {
        tempList.push({id,...temp[id]});
      }
      setTempList(tempList)
      //console.log('mapped temp: ', tempList)
    },)


  },[]);
   //render amount row in amount picker base on amount in deviceType
   const renderRow = (row) => {
     const arr = []
      for(let i=1; i<=row ; i++) {
        arr.push(<Picker.Item label={i+""} value={i+""} key={i}/> )
      }
      return arr
   }
   

   //card
   //set amount of device that can be borrow from selected deviceType
   const setAmount = (type) =>  {
   deviceRef.child('deviceType').child(type).on('value',(snapshot) =>{
    const ref = snapshot.val();
    //console.log("REF of ",selectedType," >> ",ref)
    let i = 0;
    for(let key in ref){
      //console.log(key)
     i++
    }
    //console.log(i)
   setCountAmount(i)
   
  },)
}
   
  // adding requesting device to RTB
   const addRequestingToFirebase = (type,amount,location,selectdate) => {
    const tempBorrowRef = userRef.child(user.uid).child('Items')
    .child('requesting')
    
    tempBorrowRef.push({
      deviceType: type,
      location : location,
      amount: amount,
      requestingDate: moment(date).add(1,'days').toString(),
      requestingStatus: 'pending',
      borrowerName: name,
      borrowerTel: tel,
      borrowerUID: user.uid
    })
    
    setSelectedType('0')
    setSelectedAmount('0')
    setSelectedLocation('0')
    
   }
   //NOTI
   const sendNotification = () => {
     pushNoti("admin",name+" has submitted a device request form")
   }
   // delete 
   const deleteCard = (item) => {
    const tempBorrowRef = userRef.child(user.uid).child('Items')
    .child('borrow_temp').child(item.id)
    tempBorrowRef.remove()
   }
   //save noti to database
   const saveNotificationToDatabase = () => {
      userRef.once('value',(snapshot) => {
        const userSnap = snapshot.val()
        for(let key in userSnap) {
          if(userSnap[key].userRole == 'admin') {
            console.log(key)
            const NotiRef = userRef
            .child(key)
            .child("Notification")
            .child(dayjs(Date()).format("YYYY/MM/DD"))
            .push({
              message:name+" has submitted a device request form",
              status:"unread",
              timeStamps: dayjs(Date()).format("YYYY/MM/DD hh:mm:ss A")
            })
          }
        }
      })
   }

  const [date, setDate] = useState(Date.parse(moment().format('L, LT')))
  const [selectedDate, setSelectedDate] = useState(
    Date.parse(moment().format('L, LT')),
  )

  const [mode, setMode] = useState('date')
  const [show, setShow] = useState(false)

  const checkDeviceType = selectedType != '0'
  const checkAmount = selectedAmount != '0'
  const checkLocation = selectedLocation != '0'
  const checkDate = selectedDate != date

  // console.log('checkDeviceName', checkDeviceName)
  // console.log('checkAmount', checkAmount)
  // console.log('checkLocation', checkLocation)
  // console.log('checkDate', checkDate)

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date
    setShow(Platform.OS === 'ios')
    //console.log("Current date",currentDate)
    setDate(currentDate)
    setShow(false)
  }

  const showMode = (currentMode) => {
    setShow(true)
    setMode(currentMode)
  }

  const showDatepicker = () => {
    showMode('date')
  }

  const showTimepicker = () => {
    showMode('time')
  }

  

  return (
      <View style={{ flex: 1, paddingBottom: 24 }}>
        <View>
          <View style={{ marginTop: 24, marginLeft: 8, marginRight: 8 }}>
            {/* divce name picker */}
            <View>
              <Text style={styles.pickerLabel}> Device Type </Text>
              <Picker
                style={{ height: 50, width: '100%' }}
                mode="dropdown"
                selectedValue={selectedType}
                onValueChange={(itemValue) => {setSelectedType(itemValue), console.log("itemValue >> ",itemValue),setAmount(itemValue)}}>
                {/* onValueChange={(itemValue) => {
               if (itemValue != '0') {
                 setSelectedValue(itemValue)
               }
             }}> */}
            <Picker.Item label="select device" value="0" />
           
           
            {/*maping*/}
            
            {DeviceTypeList ? DeviceTypeList.map((types, index) => 
            
            
            <Picker.Item label={types.type} value={types.type} key={index}/> ) : '' }
              </Picker>
              
            </View>
            {/* amount picker */}
            
            <View style={{ marginTop: 16 }}>
              <Text style={styles.pickerLabel}> Amount </Text>
              <Picker
                style={{ height: 50, width: '100%' }}
                mode="dropdown"
                selectedValue={selectedAmount}
                onValueChange={(itemValue) => setSelectedAmount(itemValue)}
              >
                  <Picker.Item label="select amount" value="0" />
                {/*
                {devices && selectedType != "0" ? renderRow(devices.find((value) => {
                
                if(value.model === selectedType) {
                  //console.log("this is value : " , value)
                  return value}
                }).amount
                ): null}
              */}
              
              {renderRow(countAmount)}
              
                
               


            
              </Picker>
                              
            </View>
            
            {/* location picker */}
            
            <View style={{ marginTop: 16 }}>
              <Text style={styles.pickerLabel}> Location </Text>
              <Picker
                style={{ height: 50, width: '100%' }}
                mode="dropdown"
                selectedValue={selectedLocation}
                onValueChange={(itemValue) => {setSelectedLocation(itemValue)}}>


                <Picker.Item label="select location" value="0" />
                {locations ? locations.map((location, index) => 
            
                <Picker.Item label={location.name} value={location.name} key={index}/> ) : '' }
              
              </Picker>
            </View>
            <View style={{flexDirection:'row'}}>
            <View style={{flexDirection:'column',flex:1}}>
            <Text style={styles.pickerLabel}> Date {moment().add(1,'days').format("YYYY/MM/DD")}</Text>
            
            {/*<TouchableOpacity
                style={{ borderColor: 'black', height: 50 }}
                onPress={showDatepicker}>
                <View style={{}}>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={{ marginLeft: 8, marginTop: 16, fontSize: 16 }}>
                      {moment(date).format('L')}
                    </Text>
                  </View>
                  <View style={{ alignSelf:'flex-end' }}>
                    <Icon
                      name="calendar-outline"
                      type="ionicon"
                      color="#b3b3b3"
                      size={32}
                      iconStyle={{ marginTop: 8, marginRight: 8 }}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'column',flex:1}}>
            <Text style={styles.pickerLabel}> Time </Text>
            <TouchableOpacity
                style={{ borderColor: 'black', height: 50 }}
                onPress={showTimepicker}>
                <View style={{ flexDirection: 'column', }}>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={{ marginLeft: 8, marginTop: 16, fontSize: 16 }}>
                      {moment(date).format('h:mm a')}
                    </Text>
                  </View>
                  <View style={{ alignSelf: 'flex-end' }}>
                    <Icon
                      name="timer-outline"
                      type="ionicon"
                      color="#b3b3b3"
                      size={32}
                      iconStyle={{ marginTop: 8, marginRight: 8 }}
                    />
                  </View>
                </View>
              </TouchableOpacity>
              {/*show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  // minimumDate={new Date(moment().format("YYYY"),moment().format("MM"),moment().add(1,'days').format("DD"))}
                  minimumDate={new Date(moment().add(1,'days'))}
                  value={date}
                  mode={mode}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )*/}
            </View>
            </View>
            {
            <View style={{ marginTop: 16 }}>

              <Text style={styles.pickerLabel}> Time </Text>
              <TouchableOpacity
                style={{ borderColor: 'black', height: 50 }}
                onPress={showTimepicker}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={{ marginLeft: 8, marginTop: 16, fontSize: 16 }}>
                      {moment(date).format('h:mm a')}
                    </Text>
                  </View>
                  <View style={{ alignSelf: 'center' }}>
                    <Icon
                      name="timer-outline"
                      type="ionicon"
                      color="#b3b3b3"
                      size={32}
                      iconStyle={{ marginTop: 8, marginRight: 8 }}
                    />
                  </View>
                </View>
              </TouchableOpacity>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  minimumDate={new Date(moment().format("YYYY"),moment().format("MM"),moment().add(1,'days').format("DD"))}
                  value={date}
                  mode={mode}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )}
            </View>
              }
             {/*Confirm Button*/ }
            <View
              style={{
                marginTop: 24,
                width: '60%',
                alignSelf: 'center',
              }}>
              <Button
                title="confirm"
                disabled={
                  !checkDeviceType ||
                  !checkAmount ||
                  !checkLocation ||
                  !checkDate
                } 
                onPress={() =>
                  {console.log("ADD >>> Type: ", selectedType, " Amount: ", selectedAmount, " Location: ", selectedLocation, " Date: ", moment(date).format('YYYY/MM/DD : h:mm a'))
                  Alert.alert(
                    'Confirm',
                    'Add this device to requesting list?',
                    [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel'
                      },
                      { text: 'OK',
                       onPress: () =>{ addRequestingToFirebase(selectedType,selectedAmount,selectedLocation,date);
                        sendNotification(),saveNotificationToDatabase(),
                        Alert.alert(
                          '',
                          "COMPLETE",[{
                            text:'OK',
                            onPress: () => navigation.goBack()
                          }]
                        )
                      }}
                    ],
                    { cancelable: false })
                  
                }}
                />
            </View>
          
          </View>
        </View>  
         
         
          
          
          
          
          
        
        
      </View>
  
  )
}

export default BorrowerDeviceRequest

const styles = StyleSheet.create({
  pickerLabel: {
    color: 'grey',
  },
  cardView: {
    flex: 1,
    marginTop: 24,
    flexDirection: 'row',
  },
  cardHeader: {
    color: 'grey',
    fontSize: 16,
    width: '32%',
  },
  cardContent: {
    fontSize: 16,
  },
})
