import React ,{useState,useEffect} from "react";
import { View, Text, StyleSheet,TouchableOpacity,FlatList,SafeAreaView} from "react-native";
import auth from '@react-native-firebase/auth'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {db,userRef} from '../../../configs/firestore'
import { Button } from "native-base";
import DateTimePicker from '@react-native-community/datetimepicker'
import { Picker } from '@react-native-community/picker'
import moment from 'moment'
import {map} from 'lodash'
import { Card } from 'react-native-elements'
const BorrowerDeviceBorrowHistory = () => {
    const user = auth().currentUser;
    const [historyList,setHistoryList] = useState([]);
    const [selectHistoryList,setSelectHistoryList] = useState([])
    const [selectedDate, setSelectedDate] = useState(moment().format('YYYY/MM/DD'))
      
    const [date, setDate] = useState(Date.parse(moment().format('L')))
    const [setectedDay,setSelectedDay] = useState('')
    const [setectedMonth,setSelectedMonth] = useState('')
    const [setectedYear,setSelectedYear] = useState('')
    const [mode, setMode] = useState('date')
    const [show, setShow] = useState(false)
    
useEffect(() => {

    const  UserRef = userRef.child(user.uid)
     
     const HistoryRef = UserRef.child('History/BorrowingHistory').child(moment(date).format('YYYY/MM/DD')).child(user.uid)
     //console.log("HistoryRef >> ",HistoryRef)
     HistoryRef.on('value',(snapshot) => {
       const history = snapshot.val();
       //console.log("history >> ",history)
       const HistoryList = [];
       
       setHistoryList(HistoryList)
      // console.log('mapped history: ', historyList)
    
      for(let key in history) {
          console.log(key)
         // HistoryList.push({"UID":key,...history[key]})
         HistoryList.push(history[key])
      } 

      setSelectHistoryList(HistoryList)
      console.log(HistoryList)
     })
     
     
   },[date]);

   const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date
    
    setShow(Platform.OS === 'ios')
    setDate(currentDate)
    //console.log(moment(date).format('YYYY/MM/DD'))
    //setSelectedDate(moment(date).format('YYYY/MM/DD'))
     //console.log(date)
    setShow(false)
  }
   const showMode = (currentMode) => {
    setShow(true)
    setMode(currentMode)
  }
   const showDatepicker = () => {
    showMode('date')
  }
   
return(
    <View style={{flex:1, backgroundColor: 'white'}}>
        <View style={{ backgroundColor: 'white', flexDirection:'row'} }>
        <TouchableOpacity 
                onPress={showDatepicker}>
        <Text style={styles.sectionHeader}>device borrow history of </Text>
        {/*<Text style={styles.sectionHeader}>{setectedDate}</Text>*/}
        
                <View style={ styles.sectionHeader}>
                  <View>
                    <Text>
                      {moment(date).format('DD/MM/YYYY')}
                    </Text>
                  </View>
                  
                </View>
              </TouchableOpacity>
              
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={mode}
            is24Hour={true}
            display="spinner"
            onChange={onChange}
          />
        )}
        
        
        </View>
        
              {/*console.log(selectHistoryList)*/}
        {!!selectHistoryList.length &&
        selectHistoryList.map((Device, index) => (
          console.log(index ," : ",Device),
          <TouchableOpacity
            key={Device.key}
            >
            <Card containerStyle={{borderRadius: 10, elevation: 8}}>
              <View
                style={styles.card}
                  
                >
                <View>
                <Text style={{ marginBottom: 8 }}>
                    {Device.UID.split("/")[0]} 
                  </Text>
                  <Text style={{ marginBottom: 8 }}>
                    Brand : {Device.deviceBrand}
                  </Text>
                  <Text style={{ marginBottom: 8 }}>
                    S/N {Device.deviceSerialNumber}
                  </Text>
                  <Text style={{ marginBottom: 8 }}>
                  Model: {Device.deviceModel|| "-"}
                  </Text>
                  <Text style={{ marginBottom: 8 }}>
                    return Time:  {Device.returningTime || '-'}
                  </Text>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
        ))}
        {/*<SafeAreaView style={styles.container}>
    {selectHistoryList?selectHistoryList.map((item,index) =>
        <FlatList
        data={item}
        renderItem={({item}) =>
        <Text>{item.model}</Text>}
        />
        ):''}
    </SafeAreaView>    */}
   </View>
 
    
            
    )

}

    



const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
    actionButtonIcon: {
      fontSize: 20,
      height: 22,
      color: 'white',
    },
    approved:{
      color: '#28CD1A'
    },
    rejected:{
      color: '#ff0000'
    },
    sectionHeader: {
      color: 'grey',
      textTransform: 'uppercase',
      fontSize: 15,
      marginLeft: 16,
      marginTop: 16,
      marginRight: 16
    },
  })
export default BorrowerDeviceBorrowHistory