import React from 'react'
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Button,
  Alert,
  Dimensions,
} from 'react-native'

import { Card, Icon } from 'react-native-elements'
import ActionButton from 'react-native-action-button'

import { useNavigation } from '@react-navigation/native'
import { useEffect, useState } from 'react'
//import firebase
import { db, userRef } from '../../../configs/firestore'
import auth from '@react-native-firebase/auth'
import moment from 'moment'
import { ScrollView } from 'react-native-gesture-handler'
import { BlurView } from '@react-native-community/blur'
const BorrowerHome = () => {
  const navigation = useNavigation()
  //firebase
  const user = auth().currentUser
  const [temp, setTempList] = useState([])
  const [usedDeviceList, setUsedDeviceList] = useState([])

  const deleteCard = (item) => {
    const tempBorrowRef = userRef
      .child(user.uid)
      .child('Items')
      .child('requesting')
      .child(item.id)
    tempBorrowRef.remove()
  }
  useEffect(() => {
    const UserRef = userRef.child(user.uid)
    const Borrowed_itemsRef = UserRef.child('Items')
    const tempBorrowRef = Borrowed_itemsRef.child('requesting')
    tempBorrowRef.on('value', (snapshot) => {
      const temp = snapshot.val()
      const tempList = []
      for (let id in temp) {
        tempList.push({ id, ...temp[id] })
      }
      setTempList(tempList)
      // console.log('mapped temp: ', tempList)
    })
    const inUseRef = Borrowed_itemsRef.child('In-use')
    inUseRef.on('value', (snapshot) => {
      const used = snapshot.val()
      const usedList = []
      //console.log('USED >> ', used)
      for (let key in used) {
        // console.log('EACH > ', used[key])
        for (let subkey in used[key]) {
          //console.log('LOG >>>>', used[key][subkey])
          //console.log(subkey)
          usedList.push({ deviceType: key, ...used[key][subkey] })
        }
        //
      }
      setUsedDeviceList(usedList)
      //console.log('UDL >>', usedDeviceList)
    })
  }, [])

  return (
    //#f3f3f3
    <View style={{ flex: 1, backgroundColor: 'white', zIndex: 999 }}>
      <Text style={styles.sectionHeader}> BORROWING </Text>
      <ScrollView contentContainerStyle={{paddingBottom: 16}}>
        {usedDeviceList
          ? usedDeviceList.map((useddevice, index) => (
              //console.log("UD loop",useddevice),
              <TouchableOpacity
                key={index}
                onLongPress={() => {
                  console.log(useddevice.deviceSerialNumber)
                }}>
                <Card containerStyle={{ borderRadius: 10, elevation: 8 }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      
                      <Text style={{ lineHeight: 24,fontSize:16 }}>
                        {useddevice.deviceType} 
                      </Text>
                      <View style={{flexDirection:'row'}}>
                        <Text style={{ lineHeight: 20,fontSize:12 ,color:'#757575'}}>
                         S/N {" "}
                        </Text>
                        <Text style={{ lineHeight: 20,fontSize:12 }}>
                          {useddevice.deviceSerialNumber}
                        </Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                <Text style={{ lineHeight: 20 ,fontSize:12,color:"#757575"}}>
                   Patient Name:
                </Text>
               {useddevice.patientName ? <Text style={{ lineHeight:20,fontSize:12,color:'#000000'}}>
               {useddevice.patientName || "-"} , HN {useddevice.patientHospitalNumber|| "-"}
                </Text> : <Text> - </Text>}
                </View>
                      <View style={{flexDirection:'row'}}>
                      <Text style={{ lineHeight: 20 ,fontSize:12,color:"#757575"}}>
                   Location: {" "}
                </Text>
                      <Text style={{ lineHeight: 20 ,fontSize:12 }}>
                       {useddevice.currentLocation}
                      </Text>
                      </View>
                      {/*<Text style={{ marginBottom: 8 }}>Date: {useddevice.borrowingDate} </Text>*/}
                    </View>
                    {/*
                  <View>
                    <Text style={{ color: '#28CD1A' }}>
                      {useddevice.currentStatus}
                    </Text>
                  </View>
                  */}
                  </View>
                </Card>
              </TouchableOpacity>
            ))
          : ''}
      </ScrollView>

      <Text style={styles.sectionHeader}> Requesting </Text>
      <ScrollView style={{ backgroundColor: 'white', maxHeight: 400 }} contentContainerStyle={{paddingBottom: 16}}>
        {temp
          ? temp.map((tempDevice, index) => (
              <TouchableOpacity
                key={index}
                onLongPress={() => {
                  console.log('u long press')
                  Alert.alert(
                    'Confirm',
                    'Delete this requesting device?',
                    [
                      {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                      },
                      { text: 'OK', onPress: () => deleteCard(tempDevice) },
                    ],
                    { cancelable: false },
                  )
                }}>
                <Card
                  containerStyle={{
                    borderRadius: 10,
                    elevation: 8,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      <Text style={{ marginBottom: 8 }}>
                        {tempDevice.deviceType}
                      </Text>
                      <Text style={{ marginBottom: 8 }}>
                        amount: {tempDevice.amount}
                      </Text>
                      <Text>
                        Date :{' '}
                        {moment(tempDevice.requestingDate).format(
                          'DD/MM/YYYY',
                        ) +
                          ' ' +
                          moment(tempDevice.requestingDate).format('h:mm a')}
                      </Text>
                    </View>
                    <View>
                      <Text style={{ color: '#FFD700' }}>
                        {tempDevice.requestingStatus}
                      </Text>
                    </View>
                  </View>
                </Card>
              </TouchableOpacity>
            ))
          : ''}
      </ScrollView>

      {/* Floating Action Button */}
      {/*<View style={{ flex: 1 , backgroundColor:'gray'}}>
        <ActionButton buttonColor="#2C9086" backdrop={true} size={20}>
          <ActionButton.Item
            
            buttonColor="#EB5757"
            title="CHECKOUT BORROWING"
            onPress={() => {navigation.navigate(''),console.log("Checkout borrowing FAB press")}}>
            <Icon
              name="arrow-up-outline"
              type="ionicon"
              style={styles.actionButtonIcon}
              color="white"
              
            />
          </ActionButton.Item>
          <ActionButton.Item
            
            buttonColor="#F1B46C"
            title="DEVICE REQUEST"
            onPress={() => { navigation.navigate('DeviceRequest'),console.log("Device request FAB press")}}>
            <Icon
              name="hand-left"
              type="ionicon"
              color="white"
              style={styles.actionButtonIcon}
            />
          </ActionButton.Item>
        </ActionButton>
  </View>*/}

      <ActionButton
        buttonColor="#2C9086"
        backdrop={
          <BlurView blurType="dark" style={styles.blur} blurAmount={3} />
        }>
        <ActionButton.Item
          buttonColor="#ffffff"
          title="VIEW INFO"
          onPress={() => {
            navigation.navigate('DeviceViewerQRcodeScanner'),
              console.log('QR Press')
          }}>
          <Icon
            name="md-qr-code-outline"
            type="ionicon"
            color="black"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#EB5757"
          title="CHECKOUT BORROWING"
          onPress={() => {
            navigation.navigate('FirstCheckoutBorrowingForm'),
              console.log('Checkout borrowing FAB press')
          }}>
          <Icon
            name="arrow-up-outline"
            type="ionicon"
            style={styles.actionButtonIcon}
            color="white"
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#F1B46C"
          title="DEVICE REQUEST"
          onPress={() => {
            navigation.navigate('Device Request'),
              console.log('Device request FAB press')
          }}>
          <Icon
            name="hand-left"
            type="ionicon"
            color="white"
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
      </ActionButton>
    </View>
  )
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  sectionHeader: {
    color: 'grey',
    textTransform: 'uppercase',
    marginLeft: 16,
    marginTop: 16,
  },
  blur: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
})

export default BorrowerHome
