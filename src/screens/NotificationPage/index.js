import auth from '@react-native-firebase/auth'
import React, { useEffect,useState } from 'react'
import { View, Text, Alert, Button,ScrollView,RefreshControl } from 'react-native'
import { Card, Icon } from 'react-native-elements'
import { userRef } from '~/configs/firestore'
import { INIT_USER } from '~/Functions'
import {pushNoti,LocalNotification, scheduleNotification} from '../../configs/pushNotification'
import dayjs from 'dayjs'
// import messaging from '@react-native-firebase/messaging'


const NotificationPage = () => {
  const [borrowerName, setborrowerName] = useState('0')
  const [borrowerTel, setborrowerTel] = useState('0')
  const [NotificationList,setNotificationList] = useState([])
  const [OverDueNoti,setOverDueNoti] = useState([])
  const [refreshing,setRefreshing] = useState(false)
  let [BorrowerOverDue,setBorrowerOverDue] = useState(0)
  let [overDueDevice,setOverDueDevice] = useState(0)
  const arr = []
  let relativeTime = require('dayjs/plugin/relativeTime')

    const onRefresh = () => {
      setRefreshing(true);

      wait(1000).then(() => setRefreshing(false));
    }

    const wait = (timeout) => {
      return new Promise(resolve => {
        setTimeout(resolve, timeout);
      });
    }

  const fetchOverDue = () => {
    let overDueBorrower = 0
    const NotiArray = []
    userRef.child(auth().currentUser.uid).once('value',(snapshot)=>{
      if(snapshot.val().userRole == "admin") {
        userRef.once('value',(snapshot) => {
          const idRef = snapshot.val()
          for(let id in idRef)
          userRef.child(id).once('value',(snapshot) => { 
            if(snapshot.val().userRole == "borrower") {
            userRef.child(id).child("Items").once('value',(snapshot) => {
              const itemRef = snapshot.val()
              for(let item in itemRef) {
                if(item == "In-use") {
                  console.log("true")
                  overDueBorrower++
                }
              }
            })}})})
          
            setBorrowerOverDue(overDueBorrower)
            if(BorrowerOverDue > 1) {
              NotiArray.push({message:overDueBorrower+" Borrowers has not returned a medical device","timeStamps":"today"})
            }
            if(BorrowerOverDue = 1) {
              NotiArray.push({message:overDueBorrower+" Borrower has not returned a medical device","timeStamps":"today"})
            }
          }
      
      if(snapshot.val().userRole == "borrower") {
        userRef.child(auth().currentUser.uid).child("Items/In-use").once('value',(snapshot)=>{
          console.log("1111111 >>> ",snapshot.val())
          const inuseRef = snapshot.val()
          for(let deviceType in inuseRef) {
            console.log(deviceType)
            for(let device in inuseRef[deviceType]) {
              console.log(device)
              overDueDevice++
            }
          }
        
    })
          NotiArray.push({message:"You has "+overDueDevice+ " medical device that has not returned","timeStamps":"today"})
        
    }
      console.log(arr)
  })
    setOverDueNoti(NotiArray)
  }

  const saveNotificationToDatabase = (message) => {
    userRef.once('value',(snapshot) => {
      const userSnap = snapshot.val()
      for(let key in userSnap) {
        if(userSnap[key].userRole == 'admin') {
          console.log(key)
          const NotiRef = userRef
          .child(key)
          .child("Notification")
          .child(dayjs(Date()).format("YYYY/MM/DD"))
          .push({
            message:message,
            status:"unread",
            timeStamps: dayjs(Date()).format("YYYY/MM/DD hh:mm:ss A")
          })
        }
      }
    })
 }

  const fetchNotification = () => {
    //const arr = []
    userRef.child(auth().currentUser.uid).once('value',(snapshot=> {
      if(snapshot.val().userRole == "admin") {
        userRef
        .child(auth().currentUser.uid)
        .child("Notification")
        .child(dayjs(Date())
        .format("YYYY/MM"))
        //.orderByPriority()
        .on('value',(snapshot) => {
          const NotiRef = snapshot.val()
          //console.log(NotiRef)
          for(let day in NotiRef) {
            //console.log(NotiRef[day])
            for(let item in NotiRef[day])
            
            arr.push({...NotiRef[day][item]})
            console.log(arr)
          }
        })
        setNotificationList(arr)
      }
    }))
    
    
    
    
  }
  useEffect(() => {
    //scheduleNotification("admin","Hi")
    INIT_USER(auth().currentUser.uid,setborrowerName,setborrowerTel)
    fetchOverDue()
    fetchNotification()
    
    
    //setNotificationList(arr)
  },[refreshing])
  
  return (
    <ScrollView style={{ flex: 1, backgroundColor: '#ffffff' }} refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh}/>}>
     {OverDueNoti.length ?
      OverDueNoti.map((item) => (
      <Card
        containerStyle={{
          borderRadius: 10,
          borderWidth: 0,
          elevation: 8,
          marginBottom: 16,
        }}
        wrapperStyle={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{ width: '80%' }}>
          <View>{/*
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
              Jiramed Withunsappasiri
              <Text style={{ fontSize: 18, fontWeight: 'normal' }}>
                {' '}
                has not return 1 device
              </Text>
          </Text>*/}
          <Text>{item.message}</Text>
          </View>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#2C9086' }}>
            {item.timeStamps == "today" ? "today": dayjs.extend(relativeTime)
            (item.timeStamps).fromNow()}
          </Text>
        </View>
        {/*
        <View style={{ marginRight: 8 }}>
          {item.status == "unread" ?                                                                                           
          <Icon name="ellipse" type="ionicon" color="#2C9086" size={16} />
          : null}
        </View>*/}
      </Card>
      ))
      : null}
      
      {NotificationList.length ?
      NotificationList.map((item) => (
      <Card
        containerStyle={{
          borderRadius: 10,
          borderWidth: 0,
          elevation: 8,
          marginBottom: 16,
        }}
        wrapperStyle={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <View style={{ width: '80%' }}>
          <View>{/*
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
              Jiramed Withunsappasiri
              <Text style={{ fontSize: 18, fontWeight: 'normal' }}>
                {' '}
                has not return 1 device
              </Text>
          </Text>*/}
          <Text>{item.message}</Text>
          </View>
          <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#2C9086' }}>
            {item.timeStamps == "today" ? "today": dayjs.extend(relativeTime)
            (item.timeStamps).fromNow()}
          </Text>
        </View>
        {/*
        <View style={{ marginRight: 8 }}>
          {item.status == "unread" ?                                                                                           
          <Icon name="ellipse" type="ionicon" color="#2C9086" size={16} />
          : null}
        </View>*/}
      </Card>
      ))
      : null}

    </ScrollView>
    
  )
}

export default NotificationPage
