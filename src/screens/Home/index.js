/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React,{useState, useEffect} from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native'
import { connect } from 'react-redux'

import selector from '~/redux/example/selector'
import { exampleAction, examplePayloadAction } from '~/redux/example/action'

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen'
/* A */

// test firestore 
import  {watchPersonData} from '../../redux/firestore_redux/firestore.action'
import { db, deviceRef, personRef } from '~/configs/firestore'
import { log } from 'react-native-reanimated'

/*
const mapStateToProps = (state) => {
  return {
    personData: state.personData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    watchPersonData: () => dispatch(watchPersonData())
  }
}
*/


personRef.on('value', (snapshot) => {
  //console.log(snapshot.val());
})

deviceRef.on('value', (snapshot) => {
//  console.log(snapshot.val())
  const devices = snapshot.val();
  const deviceNameList= [];
  for (let id in devices) {
    deviceNameList.push(devices[id].model)
    //console.log("hi")
    
  }
  //console.log(deviceNameList)
 // console.log(devices)
})

const test2 = "test"


const App = (props) => {
  
  useEffect(() => {
    const personRef = db.ref("person");
    const deviceRef = db.ref("devices")
    deviceRef.on('value', (snapshot) => {
      const devices = snapshot.val();
      const deviceList = [];
      //try to map
      const deviceNameList= [];
      
      for (let id in devices) {
        deviceList.push(devices[id]);
        
      }
      for (let name in devices) {
        deviceNameList.push(devices[name])
       // console.log("hi")
      }
      //console.log(deviceList);
      //console.log(deviceNameList)
    });
  },[]);
  
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Button title="action" onPress={props.exampleAction} />
              <Button
                title="action with payload"
                onPress={() => props.examplePayloadAction('ควย')}
              />
             
              
              <Text>{props.example.someData}</Text>
              
              <Text style={styles.sectionTitle}>Step One</Text>
              <Text style={styles.sectionDescription}>
                Edit <Text style={styles.highlight}>App.js</Text> to change this
                screen and then come back to see your edits.
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>See Your Changes</Text>
              <Text style={styles.sectionDescription}>
                <ReloadInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Debug</Text>
              <Text style={styles.sectionDescription}>
                <DebugInstructions />
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Learn More</Text>
              <Text style={styles.sectionDescription}>
                Read the docs to discover what to do next:
              </Text>
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
})

const mapStateToProps = (state) => ({ 
  example: selector.example(state),
  personData : state.personData })


const mapDispatchToProps = (dispatch) => {
  
  return {
    watchPersonData: () => dispatch(watchPersonData()),
    exampleAction,
    examplePayloadAction,
  };

}

export default connect(mapStateToProps, mapDispatchToProps)(App)
