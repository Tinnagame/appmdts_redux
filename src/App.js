import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, LogBox, StatusBar } from 'react-native'
import { Provider } from 'react-redux'
import auth from '@react-native-firebase/auth'
import configureStore from '~/configs/configureStore'
import Router from '~/Router'
import {
  LocalNotification,
  pushNoti,
  scheduleNotification,
} from '~/configs/pushNotification'
import {cancelLocalNotifications} from 'react-native-push-notification'
import { userRef } from './configs/firestore'
import { INIT_USER } from '../src/Functions'
import messaging from '@react-native-firebase/messaging'
//test firestore
//import {store} from './redux/firestore_redux/firestore.store'
const store = configureStore()

const App = () => {
  const [isLoadApp, setIsLoadApp] = useState(true)
  const [isAuth, setIsAuth] = useState(false)
  const [overDueDevice, setOverDueDevice] = useState(0)
  const [userName, setUserName] = useState('0')
  const [userTel, setUserTel] = useState('0')
  const [userRole,setUserRole] = useState('')
  const [uid,setUid] =useState(' ')
  const [newRequest,setNewRequest] = useState(false)
  const [borrowerOverDue,setBorrowerOverDue] = useState(0)
  //disable yellow warning box
  LogBox.ignoreLogs(['Warning: ...']) // Ignore log notification by message
  LogBox.ignoreAllLogs() //Ignore all log notifications

  //fetch overdue
  const fetchingBorrowerOverDue = async () => {
    let overDue = 0
    await userRef
      .child(auth().currentUser.uid)
      .child('Items/In-use')
      .once('value', (snapshot) => {
        //console.log("snap>> ",snapshot.val())
        const inUseRef = snapshot.val()
        for (let deviceType in inUseRef) {
          for (let device in inUseRef[deviceType]) {
            //console.log(device)
            overDue++
          }
        }
      })
    setOverDueDevice(overDue)
    //console.log(overDueDevice)
  }
  // fetch to input overdue
  const fetchOverDue = () => {
    let overDueBorrower = 0
    let overDueDevice = 0
    userRef.child(auth().currentUser.uid).once('value',(snapshot)=>{
      if(snapshot.val().userRole == "admin") {
        userRef.once('value',(snapshot) => {
          const idRef = snapshot.val()
          for (let id in idRef)
            userRef.child(id).once('value', (snapshot) => {
              if (snapshot.val().userRole == 'borrower') {
                userRef
                  .child(id)
                  .child('Items')
                  .once('value', (snapshot) => {
                    const itemRef = snapshot.val()
                    for (let item in itemRef) {
                      if (item == 'In-use') {
                        console.log('true')
                        overDueBorrower++
                      }
                    }
                  })
              }
            })
        })
        console.log(overDueBorrower)
        setBorrowerOverDue(overDueBorrower)
      }

      if (snapshot.val().userRole == 'borrower') {
        userRef
          .child(auth().currentUser.uid)
          .child('Items/In-use')
          .once('value', (snapshot) => {
            const inuseRef = snapshot.val()
            for (let deviceType in inuseRef) {
              console.log(deviceType)
              for (let device in inuseRef[deviceType]) {
                console.log(device)
                overDueDevice++
              }
            }
          })
        setOverDueDevice(overDueDevice)
      }
    })
        setOverDueDevice(overDueDevice)
    
  }

  const ScheduleNotification = () => {
    //console.log(userRole)

     if(userRole == "admin") {
      scheduleNotification("admin"," good morning admin, "+ borrowerOverDue + " borrower does not return a medical device")
    }
    if(userRole == "borrower") {
      console.log("OVERDUE >>  borrwoer")
      scheduleNotification
      ("admin","Good morning "+userName+", you has"+
       overDueDevice+" medical device that does not return")
    }
  }

  const fetchNotification = () => {
    userRef.once('value', (snapshot) => {
      for (let userId in snapshot.val()) {
        console.log(userId)
        userRef
          .child(userId)
          .child('Items/requesting')
          .once('child_added', (snapshot) => {
            setUid(snapshot.val().borrowerUID)
            setNewRequest(true)
          })
        if (newRequest) {
          pushNoti('admin', 'new device request form has submitted')
          break
        }
        setNewRequest(false)
      }
      /*
      userRef.child(uid).child("Items/requesting").limitToLast(1).once('child_added',(snapshot)=> {
        //console.log("SNAPPPPPPPPP >>> ",snapshot)
        pushNoti("req"+snapshot.val().borrowerUID,"admin",snapshot.val().borrowerName+" has submitted a request device form"+snapshot.val().deviceType)
        })*/
    })
  }
  const fetchNotification2 = () => {
    
    userRef.once('value',(snapshot) => {
     
     for(let userId in snapshot.val()) {
       console.log(userId)
       userRef.child(userId).child("Items/requesting").once('child_added',(snapshot)=>{
          setUid(snapshot.val().borrowerUID)
          setNewRequest(true)
          
       })
       if(newRequest) {
        pushNoti("admin","new device request form has submitted")
        setNewRequest(false)
        break
      }
       setNewRequest(false)
     }
     /*
     userRef.child(uid).child("Items/requesting").limitToLast(1).once('child_added',(snapshot)=> {
       //console.log("SNAPPPPPPPPP >>> ",snapshot)
       pushNoti("req"+snapshot.val().borrowerUID,"admin",snapshot.val().borrowerName+" has submitted a request device form"+snapshot.val().deviceType)
       })*/
   })
 }

  const Noti2 =() => {
    if(newRequest){
      pushNoti("admin","new device request form has submitted")
    }
  }
  
  const checkAuth = () => {
    if(auth().currentUser != null) {
      
    Noti2()
    fetchOverDue() 
    
    
    
    }
    else{
      return
    }
  }
  const Init_UserRole = () => {
    if(auth().currentUser){
      userRef.child(auth().currentUser.uid).once('value',(snapshot) => {
        setUserRole(snapshot.val().userRole)
      })
      INIT_USER(auth().currentUser.uid,setUserName,setUserTel)
    }
    
  }
  useEffect(() => {
    
    console.log("========================================")
    //checkAuth()
  //  fetchNotification()
   // fetchOverDue() 
   Init_UserRole()
   // ScheduleNotification()
  //  fetchingBorrowerOverDue()
    preload()
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newRequest])

  const preload = useCallback(async () => {
    const { currentUser } = await auth()
    if (currentUser) {
      // DO QUERY ROLE
      //   const reference = await database()
      //     .ref('/User/')
      //     .child('role')
      //     .once('value')
      //     .then((snapshot) => console.log('User data ', snapshot.val()))
      //   console.log('reference', reference)
    }

    setIsAuth(!!currentUser)
    setIsLoadApp(false)
    
  }, [])

  

  if (isLoadApp) {
    return (
      <View>
        <Text>Loading</Text>
      </View>
    )
  }
  return (
    <Provider store={store}>
      <StatusBar
        translucent={true}
        backgroundColor={'white'}
        barStyle={'dark-content'}
        hidden={false}
      />
      <Router isAuth={isAuth} />
    </Provider>
  )
}

export default App
