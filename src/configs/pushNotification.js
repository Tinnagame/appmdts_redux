import dayjs from 'dayjs'
import PushNotification from 'react-native-push-notification'

const pushNotification = () => {
    PushNotification.configure({
        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
          console.log('TOKEN:', token)
        },
    
        // (required) Called when a remote is received or opened, or local notification is opened
        onNotification: function (notification) {
          console.log('NOTIFICATION:', notification)
    
          // process the notification
    
          // (required) Called when a remote is received or opened, or local notification is opened
          notification.finish(PushNotification.FetchResult.NoData)
        },
        popInitialNotification:true,
        requestPermissions:true
      })
      PushNotification.popInitialNotification((notification) => {
        console.log('Initial Notification', notification);
      });
      
      PushNotification.createChannel(
        {
          channelId: 'request', // (required)
          channelName: 'Requesting', // (required)
          channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
          // soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
          importance: 4, // (optional) default: 4. Int value of the Android notification importance
          // vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
      )
      PushNotification.popInitialNotification((notification) => {
        console.log('Initial Notification',notification)
      })


      PushNotification.createChannel(
        {
          channelId: 'admin', // (required)
          channelName: 'admin', // (required)
          channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
          // soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
          importance: 4, // (optional) default: 4. Int value of the Android notification importance
          // vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
      )
      PushNotification.popInitialNotification((notification) => {
        console.log('Initial Notification',notification)
      })

      PushNotification.createChannel(
        {
          channelId: 'Admin Overdue', // (required)
          channelName: 'Admin Overdue', // (required)
          channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
          // soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
          importance: 4, // (optional) default: 4. Int value of the Android notification importance
          // vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
      )
      PushNotification.popInitialNotification((notification) => {
        console.log('Initial Notification',notification)
      })
      PushNotification.createChannel(
        {
          channelId: 'Borrower Overdue', // (required)
          channelName: 'Borrower Overdue', // (required)
          channelDescription: 'Borrower Overdue', // (optional) default: undefined.
          // soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
          importance: 4, // (optional) default: 4. Int value of the Android notification importance
          // vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        (created) => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
      )
      PushNotification.popInitialNotification((notification) => {
        console.log('Initial Notification',notification)
      })
    }
    
    export const LocalNotification = (channelID) => {
        PushNotification.localNotification({
            channelId: channelID,
            autoCancel:true,
            bigText:
            'This is local notification demo only shown,',
            subText:'Local Notification Demo',
            title: 'Local Notification Title',
            message: 'Expand me to see more',
            vibrate:true,
            vibration:300,
            //actions:'["YES","NO"]'
        })
    }
  
    export const pushNoti = (channelID,message) => {
        PushNotification.localNotification({
          
          channelId: channelID,
          message: message,
          allowWhileIdle:true
        })
}

export const scheduleNotification = (channelID,message) => {

  PushNotification.localNotificationSchedule({
    
    channelId:channelID,
    message: message, 
    date:  //dayjs().hour(15).minute(20).second(0).toDate(),
    dayjs().hour(15).minute(3).second(0).toDate(),
    allowWhileIdle:true
  })
}

export default pushNotification