import firebase from 'firebase/app'
import firestore from 'firebase/firestore'
import 'firebase/auth'
import 'firebase/functions'
import 'firebase/storage'
import 'firebase/database'

firebase.initializeApp({
  apiKey: 'AIzaSyAy6Cbe__3O8I57MY8HJGMH1sgKAypwsy8',
  authDomain: 'mdts-15ed3.firebaseapp.com',
  databaseURL: 'https://mdts-15ed3.firebaseio.com',
  projectId: 'mdts-15ed3',
  storageBucket: 'mdts-15ed3.appspot.com',
  messagingSenderId: '330188460298',
  appId: '1:330188460298:web:919d6427030f13e486aaf3',
  measurementId: 'G-PDNKPF195G',
})

//export const auth = firebase.auth()
//export const functions = firebase.app().functions('asia-east2')
//export const storage = firebase.storage()
//export const fire = firebase
const db = firebase.database()
const personRef = db.ref('person');
const deviceRef = db.ref('Devices');
const locationRef = db.ref('Location');
const borrowerRef = db.ref('Borrower');
const userRef = db.ref('User');


// way to intialize the firestore for reading and writing the data
//const db = firebase.firestore()
/*
firebase.database().ref('person').push().set({
  test: "testtest"
})
.then((doc) => {
  console.log(doc)
})
*/
export {db, personRef, deviceRef,locationRef,borrowerRef,userRef}
//export default firebase.firestore()


// firebase.firestore().collection("users").add({
//   first: "WOWOWO"
// })
// .then(function(docRef) {
//   console.log("Document written with ID: ", docRef);
// })
// .catch(function(error) {
//   console.error("Error adding document: ", error);
// });


