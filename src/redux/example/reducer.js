import ACTION from './actionTypes'

export const initialState = {
  someData: 'this is some data',
}

const exampleReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION.SOME_ACTION:
      return {
        ...state,
        someData: 'You Just Dispatch an action',
      }
    case ACTION.SOME_ACTION_WITH_PAYLOAD:
      return {
        ...state,
        someData: `You Just Dispatch an action with ${action.payload}`,
      }
    default:
      return state
  }
}

export default exampleReducer
