import ACTION from './actionTypes'

export const exampleAction = () => ({
  type: ACTION.SOME_ACTION,
})

export const examplePayloadAction = (payload) => ({
  type: ACTION.SOME_ACTION_WITH_PAYLOAD,
  payload,
})

export default {
  exampleAction,
  examplePayloadAction,
}
