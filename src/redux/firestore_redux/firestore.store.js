const { default: firestoreReducer } = require("./firestore.reducer");
const { applyMiddleware } = require("redux");
import {createStore} from 'redux'
import thunkMiddleware from 'redux-thunk'
const store = createStore(firestoreReducer,applyMiddleware(thunkMiddleware));
export {store}