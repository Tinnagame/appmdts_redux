//import fire from '/configs/firestore'
import { fire } from '../firestore'
import {db} from '../../configs/firestore'

export const testFirebaseAction = (payload) => ({
    type: ACTION.SOME_ACTION_WITH_FIREBASE,
    payload
  })
  

  const setPersonData = (personData) => {
    return {
      type: "setPersonData",
      value: personData
    };
  };
  
  const watchPersonData = () => {
    return function(dispatch) {
      
      db.ref("person").on("value", function(snapshot) {
          
          let personData = snapshot.val();
          let actionSetPersonData = setPersonData(personData);
          dispatch(actionSetPersonData);
          
      }, function(error) { console.log(error); });
    }
  };
  
  export { setPersonData,watchPersonData };