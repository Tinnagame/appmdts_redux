//const { initialState } = require("../example/reducer");
//const { default: action } = require("../example/action");
import action from './firestore.action'

const initialState = {
    personData: {}
}

const firestoreReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'setPersonData' :
            return {...state,personData:action.value};
        
        default:
            return state;
    }
}

export default firestoreReducer