import { combineReducers } from 'redux'
import example from './example/reducer'
import firestoreReducer from './firestore_redux/firestore.reducer'
export default combineReducers({
  example,firestoreReducer
})
