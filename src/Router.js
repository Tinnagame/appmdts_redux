import React, { useMemo } from 'react'
import { StatusBar, TouchableHighlightBase, Button, Text } from 'react-native'
import {
  NavigationContainer,
  getFocusedRouteNameFromRoute,
  useNavigation,
} from '@react-navigation/native'

import { createStackNavigator } from '@react-navigation/stack'

import Ionicons from 'react-native-vector-icons/Ionicons'
import { Icon } from 'react-native-elements'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
// import { View } from 'react-native-animatable'

import auth from '@react-native-firebase/auth'

import Home from '~/screens/Home'
import AdminHome from './screens/admin/AdminHome'
import RegularBorrowingForm from './screens/admin/RegularBorrowingForm'
import LoginPage from './screens/LoginPage'
import BorrowerHome from '../src/screens/borrower/BorrowerHome'
import BorrowerDeviceRequestHistory from './screens/borrower/BorrowerDeviceRequestHistory'
import BorrowerDeviceBorrowHistory from './screens/borrower/BorrowerDeviceBorrowHistory'
import NotificationPage from './screens/NotificationPage'

import ReturningForm from './screens/admin/ReturningForm'
import DeviceViewerQRcodeScanner from './screens/QRcodeScanner/DeviceViewerQRcodeScanner'
import BorrowerDeviceRequest from './screens/borrower/BorrowerDeviceRequest'
import DeviceRequestInformation from './screens/admin/DeviceRequestInformation'
import AdminDeviceRequestHistory from './screens/admin/AdminDeviceRequestHistory'
import AdminDeviceBorrowHistory from './screens/admin/AdminDeviceBorrowHistory'
import PersonHistoryInformation from './screens/admin/AdminDeviceBorrowHistory/PersonHistoryInformation'
import DeviceStatusMonitor from './screens/DeviceStatusMonitoring/ViewDeviceByType'
import DashBoard from './screens/DashBoard'
import BorrowerQRcodeScanner from './screens/QRcodeScanner/BorrowerQRcodeScanner'
import CheckoutBorrowingQRcodeScanner from './screens/QRcodeScanner/CheckoutBorrowingQRcodeScanner'
import DeviceQRcodeScanner from './screens/QRcodeScanner/DeviceQRcodeScanner'
import DeviceByTypePage from './screens/DeviceStatusMonitoring/ViewSelectTypeDevice'
import DeviceInformationPage from './screens/DeviceStatusMonitoring/DeviceInformation'
import ReturningDeviceQRcodeScanner from './screens/QRcodeScanner/ReturningDeviceQRcodeScanner'
import AdminReturnPage from './screens/admin/ReturningForm'
import Profile from './screens/Profile'
import FirstCheckoutBorrowingForm from './screens/borrower/CheckoutBorrowingForm/FirstCheckoutBorrowingForm'
import SecondCheckoutBorrowingForm from './screens/borrower/CheckoutBorrowingForm/SecondCheckoutBorrowingForm'
import DeviceInformationViewer from './screens/DeviceStatusMonitoring/DeviceInformationViewer'
import AdminInuseByBorrower from './screens/admin/AdminHome/AdminInuseByBorrower'
import RegisterPage from './screens/Register'

import { TouchableOpacity } from 'react-native-gesture-handler'
import { userRef } from './configs/firestore'
import { View } from 'react-native-animatable'
import Maintenance from './screens/DeviceStatusMonitoring/Maintenance'
import DeviceManager from './screens/DeviceStatusMonitoring/DeviceManager'
import AddDevice from './screens/DeviceStatusMonitoring/DeviceManager/AddDevice'
import RemoveDevice from './screens/DeviceStatusMonitoring/DeviceManager/RemoveDevice'
import RemoveDeviceQRcodeScanner from './screens/QRcodeScanner/RemoveDeviceQRcodeScanner'
const Stack = createStackNavigator()
//const Tab = createMaterialTopTabNavigator()
const BottomTab = createBottomTabNavigator()
//This function use to change Header Title base on route

function MaintainButton() {
  return (
    <TouchableOpacity
      style={{ paddingRight: 15 }}
      onPress={() => {
        console.log(console.log(auth().currentUser.uid))
      }}>
      <Icon name="build-outline" type="ionicon" color="black" />
    </TouchableOpacity>
  )
}
const checkRole = () => {
  console.log('Checking role.....')
  console.log(auth().currentUser.uid)
  userRef.child(auth().currentUser.uid).once('value', (snapshot) => {
    if (snapshot.val().userRole == 'admin') {
      console.log(snapshot.val().userRole)
      return true
    }
  })
}
function getHeaderInDeviceInformationViewer(route) {
  let str = route.device
  let res = str.split('/')
  return res[0]
}
function getHeaderTitle(route) {
  const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home'

  switch (routeName) {
    case 'HomeBorrower':
      return 'Home'
    case 'History':
      return 'History'
    case 'Dashboard':
      return 'Dashboard'
    case 'Notification':
      return 'Notifications'
    case 'Profile':
      return 'Profile'
    case 'Home':
      return 'Home'
    case 'Devices':
      return 'Devices'
    case 'FirstCheckoutBorrowingForm':
      return 'Checkout Borrowing'
  }
}

const AdminHistoryTab = createMaterialTopTabNavigator()

function AdminHistoryTopTab({ navigation, route }) {
  return (
    <AdminHistoryTab.Navigator
      options={{ headerShown: false }}
      tabBarOptions={{ indicatorStyle: { backgroundColor: '#2C9086' } }}>
      <AdminHistoryTab.Screen
        name="Borrow"
        options={{ title: 'Borrow' }}
        component={AdminDeviceBorrowHistory}
      />
      <AdminHistoryTab.Screen
        name="Request"
        options={{ title: 'Request' }}
        component={AdminDeviceRequestHistory}
      />
    </AdminHistoryTab.Navigator>
  )
}

const AdminTab = createBottomTabNavigator()
function AdminTabs({ navigation, route }) {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: getHeaderTitle(route),
      headerTitleAlign: 'center',
    })
  }, [navigation, route])

  return (
    <AdminTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#2C9086',
        inactiveTintColor: '#b1b1b1',
        tabStyle: {
          paddingTop: StatusBar.currentHeight,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline'
          } else if (route.name === 'Dashboard') {
            iconName = focused ? 'stats-chart' : 'stats-chart-outline'
          } else if (route.name === 'History') {
            iconName = focused ? 'timer' : 'timer-outline'
          } else if (route.name === 'Devices') {
            iconName = focused ? 'folder' : 'folder-outline'
          } else if (route.name === 'Notification') {
            iconName = focused ? 'notifications' : 'notifications-outline'
          } else if (route.name === 'Profile') {
            iconName = focused ? 'person' : 'person-outline'
          }

          // You can return any component that you like here!
          return (
            <Ionicons
              name={iconName}
              size={size}
              color={color}
              style={{ marginBottom: 16 }}
            />
          )
        },
        headerTitleAlign: 'center',
      })}>
      <AdminTab.Screen
        name="Home"
        options={{ title: 'Home' }}
        component={AdminHome}
      />
      <AdminTab.Screen
        name="History"
        setOptions={{ headerShown: false }}
        component={AdminHistoryTopTab}
      />
      <AdminTab.Screen name="Devices" component={DeviceStatusMonitor} />
      <AdminTab.Screen name="Dashboard" component={DashBoard} />
      <AdminTab.Screen name="Notification" component={NotificationPage} />
      <AdminTab.Screen name="Profile" component={Profile} />
    </AdminTab.Navigator>
  )
}

const BorrowerHistoryTab = createMaterialTopTabNavigator()

function BorrowerHistoryTopTab({ navigation, route }) {
  return (
    <BorrowerHistoryTab.Navigator
      options={{ headerShown: false }}
      tabBarOptions={{ indicatorStyle: { backgroundColor: '#2C9086' } }}>
      <BorrowerHistoryTab.Screen
        name="Borrow"
        options={{ title: 'Borrow' }}
        component={BorrowerDeviceBorrowHistory}
      />
      <BorrowerHistoryTab.Screen
        name="Request"
        options={{ title: 'Request' }}
        component={BorrowerDeviceRequestHistory}
      />
    </BorrowerHistoryTab.Navigator>
  )
}

const BorrowerTab = createBottomTabNavigator()

function BorrowerTabs({ navigation, route }) {
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: getHeaderTitle(route),
      headerTitleAlign: 'center',
    })
  }, [navigation, route])

  return (
    <BorrowerTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        header: { visible: true },
        activeTintColor: '#2C9086',
        inactiveTintColor: '#b1b1b1',
        tabStyle: {
          paddingTop: StatusBar.currentHeight,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline'
          } else if (route.name === 'Dashboard') {
            iconName = focused ? 'stats-chart' : 'stats-chart-outline'
          } else if (route.name === 'History') {
            iconName = focused ? 'timer' : 'timer-outline'
          } else if (route.name === 'Devices') {
            iconName = focused ? 'folder' : 'folder-outline'
          } else if (route.name === 'Profile') {
            iconName = focused ? 'person' : 'person-outline'
          } else if (route.name === 'Notification') {
            iconName = focused ? 'notifications' : 'notifications-outline'
          }

          // You can return any component that you like here!
          return (
            <Ionicons
              name={iconName}
              size={24}
              color={color}
              style={{ marginBottom: 16 }}
            />
          )
        },
      })}>
      <BorrowerTab.Screen
        name="Home"
        options={{ title: 'Home' }}
        component={BorrowerHome}
      />
      <BorrowerTab.Screen name="History" component={BorrowerHistoryTopTab} />
      <BorrowerTab.Screen name="Devices" component={DeviceStatusMonitor} />
      <BorrowerTab.Screen name="Dashboard" component={DashBoard} />
      <BorrowerTab.Screen name="Notification" component={NotificationPage} />
      <BorrowerTab.Screen name="Profile" component={Profile} />
    </BorrowerTab.Navigator>
  )
}

const DeviceInformationTopTab = createMaterialTopTabNavigator()

function myDeviceInformationTopTab() {
  return (
    <DeviceInformationTopTab.Navigator>
      <DeviceInformationTopTab.Screen name="DeviceInfor"></DeviceInformationTopTab.Screen>
      <DeviceInformationTopTab.Screen name="Borrowinfo"></DeviceInformationTopTab.Screen>
    </DeviceInformationTopTab.Navigator>
  )
}

function App({ isAuth }) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* initialRouteName={!isAuth ? 'Login' : 'home'*/}

        <Stack.Screen
          options={{ headerShown: false }}
          name="Login"
          component={LoginPage}
        />
        <Stack.Screen name="HomeAdmin" component={AdminTabs} />
        <Stack.Screen
          name="HomeBorrower"
          options={{ title: 'Home' }}
          component={BorrowerTabs}
        />
        <Stack.Screen
          name="Device Request Information"
          options={{ headerShown: false, headerShown: true }}
          component={DeviceRequestInformation}
        />
        <Stack.Screen
          options={{ headerShown: true }}
          name="Device Request"
          component={BorrowerDeviceRequest}
        />
        <Stack.Screen
          name="RegularBorrowingForm"
          component={RegularBorrowingForm}
        />

        <Stack.Screen
          name="FirstCheckoutBorrowingForm"
          options={{ title: 'Checkout Borrowing' }}
          component={FirstCheckoutBorrowingForm}
        />

        <Stack.Screen
          name="SecondCheckoutBorrowingForm"
          options={{ title: 'Checkout Borrowing' }}
          component={SecondCheckoutBorrowingForm}
        />

        <Stack.Screen
          name="BorrowerQRcodeScanner"
          options={{ title: 'Scan borrower QRcode' }}
          component={BorrowerQRcodeScanner}
        />
        <Stack.Screen
          name="DeviceQRcodeScanner"
          options={{ title: 'Scan device QRcode' }}
          component={DeviceQRcodeScanner}
        />

        <Stack.Screen
          name="DeviceViewerQRcodeScanner"
          options={{ title: 'Scan device QRcode' }}
          component={DeviceViewerQRcodeScanner}
        />
        <Stack.Screen
          name="RemoveDeviceQRcodeScanner"
          options={{ title: 'Scan device QRcode' }}
          component={RemoveDeviceQRcodeScanner}
        />
        <Stack.Screen
          name="CheckoutBorrowingQRcodeScanner"
          options={{ title: 'Scan device QRcode' }}
          component={CheckoutBorrowingQRcodeScanner}
        />
        <Stack.Screen
          name="DeviceByTypePage"
          options={({ route }) => ({ title: route.params.type })}
          component={DeviceByTypePage}
        />
        <Stack.Screen
          name="Maintenance"
          options={{ title: 'Maintenance' }}
          component={Maintenance}
        />
        <Stack.Screen
          name="DeviceManager"
          options={{ title: 'Device Management' }}
          component={DeviceManager}
        />
        <Stack.Screen
          name="AddDevice"
          options={{ title: 'Add Device' }}
          component={AddDevice}
        />
        <Stack.Screen
          name="RemoveDevice"
          options={{ title: 'Remove Device' }}
          component={RemoveDevice}
        />
        <Stack.Screen
          name="DeviceInformationPage"
          options={({ route,navigation }) => ({ title: route.params.type,
          headerRight: () => (
          <View>
            {route.params.userRole=="admin" ?  <TouchableOpacity style={{paddingRight:15}}
      onPress={() => {
          navigation.navigate('Maintenance',{
            devicePath:route.params.type+"/"+route.params.key,
            deviceBrand:route.params.deviceBrand,
            deviceSerialNumber:route.params.deviceSerialNumber,
            deviceModel:route.params.deviceModel,
            currentStatus:route.params.currentStatus
          })
      }}
      >
        <Icon
        name="build-outline"
        type="ionicon"
        color="black"
        />
      </TouchableOpacity> : null}
      </View>
      )})}
          component={DeviceInformationPage}
        />
        <Stack.Screen
          name="ReturningDeviceQRcodeScanner"
          options={{ title: 'Scan device QRcode' }}
          component={ReturningDeviceQRcodeScanner}
        />
        <Stack.Screen
          name="AdminReturnPage"
          options={{ title: 'Scan device QRcode' }}
          component={ReturningForm}
        />
        <Stack.Screen
          name="DeviceInformationViewer"
          options={({ route }) => ({
            title: getHeaderInDeviceInformationViewer(route.params),
          })}
          component={DeviceInformationViewer}
        />
        <Stack.Screen
          name="PersonHistoryInformation"
          options={{}}
          component={PersonHistoryInformation}
        />
        <Stack.Screen
          name="AdminInuseByBorrower"
          options={({ route }) => ({ title: route.params.userName })}
          component={AdminInuseByBorrower}
        />
        <Stack.Screen name="Register" component={RegisterPage} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
