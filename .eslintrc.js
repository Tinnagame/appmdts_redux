module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    'no-extra-boolean-cast': 0,
    semi: 0,
  },
}
